#ifndef PACKET_LAYER_APPLICATION_H
#define PACKET_LAYER_APPLICATION_H

#include <iostream>

#include <QDebug>
#include <QTreeWidget>
#include <QTreeWidgetItem>

#include <HttpLayer.h>
#include <DnsLayer.h>
#include <Packet.h>
#include <SystemUtils.h>

#include "utils_packet_info_display.h"

/**
 * \namespace pkl
 * \brief Namespace for network packet layer functions
 *
 */
namespace pkl
{
    void setHTTPRequestData(const pcpp::Packet &packet, QTreeWidget *packetInfo);
    void setHTTPResponseData(const pcpp::Packet &packet, QTreeWidget *packetInfo);
    void setDNSData(const pcpp::Packet &packet, QTreeWidget *packetInfo);

    static std::string getHttpMethod(pcpp::HttpRequestLayer::HttpMethod httpMethod);
    static std::string getHttpVersion(pcpp::HttpVersion httpVersion);

    /* DNS Flag */
    static void getDNSQueryOrResponse(uint16_t queryOrResponse, QTreeWidgetItem *flagRoot, pcpp::DnsLayer *dnsLayer);
    static void getDNSOpcode(uint16_t opcode, QTreeWidgetItem *flagRoot, pcpp::DnsLayer *dnsLayer);
    static void getDNSAuthoriative(uint16_t authoritative, QTreeWidgetItem *flagRoot, pcpp::DnsLayer *dnsLayer);
    static void getDNSTruncation(uint16_t truncation, QTreeWidgetItem *flagRoot, pcpp::DnsLayer *dnsLayer);
    static void getDNSRecursionDesired(uint16_t recursionDesired, QTreeWidgetItem *flagRoot, pcpp::DnsLayer *dnsLayer);
    static void getDNSRecursionAvailable(uint16_t recursionAvailable, QTreeWidgetItem *flagRoot, pcpp::DnsLayer *dnsLayer);
    static void getDNSZero(uint16_t zero, QTreeWidgetItem *flagRoot, pcpp::DnsLayer *dnsLayer);
    static void getDNSAnswerAuthenticate(uint16_t answerAuthenticate, QTreeWidgetItem *flagRoot, pcpp::DnsLayer *dnsLayer);
    static void getDNSNonAuthData(uint16_t authData, QTreeWidgetItem *flagRoot, pcpp::DnsLayer *dnsLayer);
    static void getDNSReplyCode(uint16_t response, QTreeWidgetItem *flagRoot, pcpp::DnsLayer *dnsLayer);

    static std::string getDNSClass(pcpp::DnsClass dnsClass);
    static std::string getDNSType(pcpp::DnsType dnsType);

    static const char *strBinaryuint16_tdots(uint16_t byte2, int start, int end);
}
#endif // PACKET_LAYER_APPLICATION_H