#ifndef PACKET_LAYER_INTERNET_H
#define PACKET_LAYER_INTERNET_H

#include <iostream>

#include <QDebug>
#include <QTreeWidget>
#include <QTreeWidgetItem>

#include <IPv4Layer.h>
#include <IPv6Layer.h>
#include <IcmpLayer.h>
#include <IgmpLayer.h>
#include <Packet.h>
#include <SystemUtils.h>

#include "utils_packet_info_display.h"

/**
 * \namespace pkl
 * \brief Namespace for network packet layer functions
 *
 */
namespace pkl
{
    void setIPv4Data(const pcpp::Packet &packet, QTreeWidget *packetInfo);
    void setIPv6Data(const pcpp::Packet &packet, QTreeWidget *packetInfo);
    void setICMPData(const pcpp::Packet &packet, QTreeWidget *packetInfo);
    void setIGMPData(const pcpp::Packet &packet, QTreeWidget *packetInfo);

    std::string getICMPType(pcpp::IcmpMessageType type);
}
#endif // PACKET_LAYER_INTERNET_H