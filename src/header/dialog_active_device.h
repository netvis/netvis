#ifndef DIALOG_ACTIVE_DEVICE_H
#define DIALOG_ACTIVE_DEVICE_H

#include <QDebug>
#include <QDialog>

namespace Ui
{
    class DialogActiveDevice;
}

class DialogActiveDevice : public QDialog
{
    Q_OBJECT

public:
    explicit DialogActiveDevice(QWidget *parent);
    ~DialogActiveDevice() final;
    void init(QStringList activeDev);

signals:
    void updateDevList(QStringList devList);

private slots:
    void on_ok_clicked();
    void on_cancel_clicked();

private:
    Ui::DialogActiveDevice *ui;
    QStringList DefaultDev;
    QString devName;
};

#endif // DIALOG_ACTIVE_DEVICE_H
