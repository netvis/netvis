#ifndef DIALOG_PCAPPLUSPLUS_ABOUT_H
#define DIALOG_PCAPPLUSPLUS_ABOUT_H

#include <QDialog>
#include <PcapPlusPlusVersion.h>

namespace Ui
{
    class DialogPcapPlusPlusAbout;
}

class DialogPcapPlusPlusAbout : public QDialog
{
    Q_OBJECT

public:
    explicit DialogPcapPlusPlusAbout(QWidget *parent = nullptr);
    ~DialogPcapPlusPlusAbout() final;

private slots:
    void on_buttonBox_accepted();

private:
    Ui::DialogPcapPlusPlusAbout *ui;
};

#endif // DIALOG_PCAPPLUSPLUS_ABOUT_H