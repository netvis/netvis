#ifndef WIDGET_PACKET_ARROW_H
#define WIDGET_PACKET_ARROW_H

#include <iostream>
#include <vector>

#include "Packet.h"

#include <QDebug>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QPainter>
#include <QWidget>

#include "packet_layer_link.h"
#include "packet_layer_internet.h"
#include "packet_layer_transport.h"
#include "packet_layer_application.h"

namespace Ui
{
    class WidgetPacketArrow;
}

class WidgetPacketArrow : public QWidget
{
    Q_OBJECT

public:
    explicit WidgetPacketArrow(QWidget *parent = nullptr);
    ~WidgetPacketArrow();

    /* initialise packet information */
    void setClientDevDetails(QStringList defaultDev);

    void setPacketNumber(int number);
    void setPacketLength(int length);
    void setPackerTimeStamp(std::string time);
    void setSrcAddress(std::string address);
    void setDstAddress(std::string address);

    void setLinkProtocol(std::string protocol);
    void setInternetProtocol(std::string protocol);
    void setTransportProtocol(std::string protocol);
    void setSessionProtocol(std::string protocol);
    void setPresentationProtocol(std::string protocol);
    void setApplicationProtocol(std::string protocol);

    void setForegroundColour(std::string colour);
    void setBackgroundColour(std::string colour);

    void setAnalysisComments(std::string comments);
    void setUserComments(std::string comments);

    void toggleAnalysisComments(bool toggle);
    void toggleUserComments(bool toggle);

    void selectPacket(bool selected);

    void mousePressEvent(QMouseEvent *event) override;

signals:
    void arrowClicked(int packetNumber);

private:
    Ui::WidgetPacketArrow *ui;

    void paintEvent(QPaintEvent *event) override;

    std::string packetNumber;
    std::string packetTimeStamp;
    std::string packetLength;
    std::string packetSrcAddress;
    std::string packetDstAddress;

    std::string packetLinkProtocol;
    std::string packetInternetProtocol;
    std::string packetTransportProtocol;
    std::string packetSessionProtocol;
    std::string packetPresentationProtocol;
    std::string packetApplicationProtocol;

    std::string foregroundColour;
    std::string backgroundColour;

    std::string analysisComments;
    std::string userCommments;

    std::string clientDevIPv4;
    std::string clientDevIPv6;

    QGraphicsScene *scene;
    bool packetSelected = false;
    bool FLIP;

    void setPacketArrow();
    // void setHostLines(QPainter &painter);
    void setPacketInformation();
    void setPacketColour(QPalette &pal);
};

#endif // WIDGET_PACKET_ARROW_H
