#ifndef CAPTURE_LIVE_DEVICE_H
#define CAPTURE_LIVE_DEVICE_H

#include <QVector>
#include <QString>

#include <PcapFileDevice.h>
#include <PcapLiveDeviceList.h>
#include <packet_layer_link.h>

class CaptureLiveDevice
{

public:
    CaptureLiveDevice();
    ~CaptureLiveDevice();

    QVector<QStringList> getAvailableDevices() const;

    QStringList getDefaultDevice() const;

    QStringList getDeviceList(QString &deviceName);

    QString getDeviceName(QStringList device) const;

    pcpp::IPv6Address getDeviceIPv6Addr(const pcpp::PcapLiveDevice &device);
};

#endif