#ifndef CAPTURE_ANALYSE_HANDSHAKES_H
#define CAPTURE_ANALYSE_HANDSHAKES_H

#include <iostream>

#include <QDebug>
#include <QThread>
//#include <QLayout>

#include <PcapLiveDeviceList.h>
#include <RawPacket.h>
#include <Packet.h>

#include <SSLLayer.h>
#include <TcpLayer.h>

//#include "widget_packet_arrow.h"

class CaptureAnalyseHandshakes : public QThread
{
    Q_OBJECT
public:
    CaptureAnalyseHandshakes();
    ~CaptureAnalyseHandshakes() final;
    void setPacketPointer(const pcpp::Packet *selectedPacket);

public slots:
    void startHandShakeAnalysis(int arrowIndex);

signals:
    void emitNewAnalysisComment(int packetIndex, std::string analysis);
    void emitAnalysisFinished();

private:
    void run() override;
    void analyseTCPThreeWayHandshake(pcpp::Packet *packet);
    void analyseTLSHandshake(pcpp::Packet *packet);

    /* pointer to packet to analyse */
    const pcpp::Packet *packet;

    int index;
    std::string analysisComment;
};

#endif //  CAPTURE_ANALYSE_HANDSHAKES_H