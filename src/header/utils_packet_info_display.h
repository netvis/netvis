#ifndef UTILS_PACKET_INFO_DISPLAY_H
#define UTILS_PACKET_INFO_DISPLAY_H

#include <QTreeWidget>
#include <QTreeWidgetItem>

/**
 * @namespace pkl
 * @brief Namespace for network packet layer functions
 *
 */
namespace pkl
{
    /**
     * @brief adds a root QTreeWidgetItem to a QTreeWidget.
     *
     * @param packetInfo
     * @param layerRoot
     * @param data
     */
    void addRoot(QTreeWidget *packetInfo, QTreeWidgetItem *layerRoot, QString data);

    /**
     * @brief adds a subroot QTreeWidgetItem to another QTreeWidgetItem.
     *
     * @param layerRoot
     * @param subRoot
     * @param data
     */
    void addSubRoot(QTreeWidgetItem *layerRoot, QTreeWidgetItem *subRoot, QString data);

    /**
     * @brief adds a child to a QTreeWidgetItem.
     *
     * @param parent
     * @param label
     * @param data
     */
    void addChild(QTreeWidgetItem *parent, QString label, QString data);

}
#endif // UTILS_PACKET_INFO_DISPLAY_H
