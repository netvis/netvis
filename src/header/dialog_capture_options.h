#ifndef DIALOG_CAPTURE_OPTIONS_H
#define DIALOG_CAPTURE_OPTIONS_H

#include <QDebug>
#include <QDialog>
#include <QTreeWidgetItem>

#include "PcapLiveDeviceList.h"
#include "PcapLiveDevice.h"
#include "WinPcapLiveDevice.h"

#include "capture_filter_BPF.h"
#include "utils_app_settings.h"
#include "utils_common.h"

namespace Ui
{
    class DialogCaptureOptions;
}

class DialogCaptureOptions : public QDialog
{
    Q_OBJECT

public:
    // explicit DialogCaptureOptions(QVector<QStringList> availableDevs,
    //                               QStringList defaultDev,
    //                               QWidget *parent = nullptr);
    explicit DialogCaptureOptions(QWidget *parent = nullptr);
    ~DialogCaptureOptions() final;

    /* session capture options */
    QString autoCaptureFileFormat;
    QString autoCaptureFile;
    bool autoNewFile;
    int afterPacketCnt;
    bool stopAfterPacketCnt;
    int afterKiloBytes;
    bool stopAfterKiloBytes;
    int afterSeconds;
    bool stopAfterSeconds;
    QString captureFilter;
    bool showComment;
    bool showAnalysis;
    bool startLiveCapture;
    QVector<QStringList> availableDevs;
    QStringList defaultDev;

    // signals:
    //     void startCaptureSignal();
    //     void setMWAvailableDev();
    //     void setMWDefaultDev(QStringList selectedDev);

protected:
    void setDialogInputTab(QVector<QStringList> availableDevs);

private slots:
    void on_startCaptureBtn_clicked();
    void on_closeDialogBtn_clicked();
    void on_startCaptureBtn_clicked(bool checked);

    /* display options */
    void on_commentCheck_clicked(bool checked);
    void on_analysisCheck_clicked(bool checked);

    /* output */
    void on_selectPcapNg_clicked(bool checked) const;
    void on_selectPcap_clicked(bool checked) const;
    void on_compressionBox_clicked(bool checked) const;
    void on_autoNewFile_clicked(bool checked);
    void on_afterPackets_clicked(bool checked);
    void on_afterSeconds_clicked(bool checked);
    void on_afterFileSize_clicked(bool checked);
    void on_spinPacketsAfter_valueChanged(int arg1);
    void on_spinSecondsAfter_valueChanged(int arg1);
    void on_spinFileSizeAfter_valueChanged(int arg1);
    void on_browseFolder_clicked();
    void on_autoCaptureFile_editingFinished();

    /* inout */
    void on_treeWidget_itemActivated(QTreeWidgetItem *item, int column);
    void on_cancelFilter_clicked();
    void on_applyFilter_clicked();
    void on_filterComboBox_editTextChanged(const QString &arg1) const;
    void on_filterComboBox_textActivated(const QString &arg1);

private:
    Ui::DialogCaptureOptions *ui;

    void init();
    void showStatusTip(const QString &statusTip);
    QStringList readRecentFilters();
    QStringList selectedDev;

    QTreeWidgetItem *devItem;
    QTreeWidgetItem *devChildIPv4;
    QTreeWidgetItem *devChildIPv6;
    QTreeWidgetItem *devChildGateway;

    /* filters */
    CaptureFilterBPF *filterBPF;
};

#endif // DIALOG_CAPTURE_OPTIONS_H
