#ifndef CAPTURE_PACKET_THREAD_H
#define CAPTURE_PACKET_THREAD_H

#include <iostream>
#include "stdlib.h"

#include <QDebug>
#include <QHBoxLayout>
#include <QStatusBar>
#include <QStandardItemModel>
#include <QFileInfo>
#include <QThread>
#include <QApplication>

#include "PcapFileDevice.h"
#include "PcapLiveDeviceList.h"
#include "RawPacket.h"

#include "utils_common.h"

class CapturePacketThread : public QThread
{
    Q_OBJECT
public:
    explicit CapturePacketThread(bool liveCapture,
                                 pcpp::RawPacketVector *rawPacketList,
                                 QVector<QString> *packetCommentList,
                                 QStatusBar *statusBar,
                                 std::string *activeDevName);

    ~CapturePacketThread() final;

    static void onPacketArrives(pcpp::RawPacket *packet,
                                pcpp::PcapLiveDevice *dev,
                                void *cookie);
    void stopLiveCapture();
    bool openCaptureFile(const QString &fileName, const QString &capturefilter = nullptr);
    void openCaptureLive(const QString &captureFilter = nullptr);
    void stopFileLoading();

    bool saveToPcapFile(const QString &savefileName);
    bool saveToPcapNgFile(const QString &savefileName, bool compression);

signals:
    void emitNewCapturePacket(int rawPacketListSize);
    void emitCaptureSessionSignal(en::threadState state);

private:
    void run() override;
    void consumePacket(pcpp::RawPacket *rawPacket);
    bool readPcapCaptureFile(pcpp::IFileReaderDevice *reader);
    bool readPcapNgCaptureFile(pcpp::PcapNgFileReaderDevice *reader);
    void setDeviceInfo(const std::string &devName);

    QStatusBar *statusBar;
    pcpp::PcapLiveDevice *dev;

    /* pointer to container for all captured/loaded packets */
    pcpp::RawPacketVector *rawPacketList;
    int lastPacketRecieved;
    /* pointer to container for packet comments */
    QVector<QString> *packetCommentList;

    std::string *activeDevName;

    /* capture thread state flags */
    bool isLiveCapture = true;
    bool isFileLoading;
    bool isFileLoadStopped;

    /* filters */
    QString capturefilter;

    // struct to hold pcapng header file details
    struct fileHeaderInfo
    {
        std::string os;          // os info - from opened pcapng file/used on save
        std::string hw;          // hw info - from opened pcapng file/used on save
        std::string app;         // app details - from opened pcapng file/used on save
        std::string filecomment; // file comment - from opened pcapng file/used on save
        int linklayer;           // link layer - from opened pcap file/used on save
        std::string devname;
        std::string devdesc;
        pcpp::IPv4Address ipv4addr;
        pcpp::MacAddress macaddr;
        pcpp::IPv4Address defaultgateway;
        std::vector<pcpp::IPv4Address> dnsservers;
        int mtu;
        std::string pcaplibver;
        pcpp::PcapLiveDevice::LiveDeviceType devtype;

        void
        clear()
        {
            os = "";
            hw = "";
            app = "";
            filecomment = "";
            // linklayer = nullptr;
        }
    };
    fileHeaderInfo headerinfo;
};

#endif // CAPTURE_PACKET_THREAD_H