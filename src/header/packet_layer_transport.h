#ifndef PACKET_LAYER_TRANSPORT_H
#define PACKET_LAYER_TRANSPORT_H

#include <iostream>

#include <QDebug>
#include <QTreeWidget>
#include <QTreeWidgetItem>

#include <TcpLayer.h>
#include <UdpLayer.h>
#include <Packet.h>
#include <SystemUtils.h>

#include "utils_packet_info_display.h"

/**
 * \namespace pkl
 * \brief Namespace for network packet layer functions
 *
 */
namespace pkl
{
    void setTCPData(const pcpp::Packet &packet, QTreeWidget *packetInfo);
    void setUDPData(const pcpp::Packet &packet, QTreeWidget *packetInfo);

    void setTCPFlags(const pcpp::TcpLayer *tcpLayer, QTreeWidgetItem *flagRoot);
    static const char *strBinaryuint12_tdots(uint16_t byte2, int start, int end);
}
#endif // PACKET_LAYER_TRANSPORT_H