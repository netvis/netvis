#ifndef UTILS_LOGGER_H
#define UTILS_LOGGER_H

#include <stdio.h>
#include <iostream>

//#include <QObject>
#include <QApplication>
#include <QTime>
#include <QDebug>
//#include <QErrorMessage>
#include <QString>
#include <QFile>
#include <QDir>
#include <QMutex>

/**
 * \namespace Logger
 * \brief Namespace for application message log handler
 *
 **/
namespace Logger
{
    /**
     * @brief message log handler which redirects Qt's qDebug output to log file and std:err
     *
     * @param type a QMssgType
     * @param context a QMessageLogContext
     * @param message a QString
     */
    void messageHandler(QtMsgType type,
                        const QMessageLogContext &context,
                        const QString &message);
}
#endif // UTILS_LOGGER_H
