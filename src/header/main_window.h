#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <stdio.h>
#include <iostream>
#include <algorithm>

#include <QDir>
#include <QDebug>
#include <QErrorMessage>
#include <QFileDialog>
#include <QMainWindow>
#include <QMessageBox>
#include <QToolBar>
#include <QScreen>
#include <QVector>
#include <QtWidgets>
#include <QLabel>
#include <QSplitter>
#include <QComboBox>
#include <QSizePolicy>

#include "capture_packet_thread.h"
#include "widget_capture_screen.h"
#include "dialog_active_device.h"
#include "dialog_capture_options.h"
#include "dialog_app_about.h"
#include "dialog_pcapplusplus_about.h"
#include "dialog_recent_files.h"
#include "capture_filter_BPF.h"
#include "capture_live_device.h"
#include "utils_app_settings.h"
#include "utils_common.h"

QT_BEGIN_NAMESPACE
namespace Ui
{
    class MainWindow;
    class QAction;
    class QMenu;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow() final;

    /* app/data state flags */
    bool isFileOpen;
    bool isFileSaved;
    bool isLiveCapture;
    bool isFileLoading;
    bool isFileSaving;
    bool isFileComment = false; // flag set when there are capture comments

    void loadFile(const QString fileName);

    // status bar info get/set'ers
    void hide_sbPacketInfo();
    void show_sbPacketInfo();
    void hide_sbCurrentSource();
    void show_sbCurrentSource();
    void show_sbCurrentDevice();
    void hide_sbCurrentDevice();
    void set_sbCurrentDevice(QStringList defaultDev);
    void set_sbCurrentSource(QString message, bool isFile);

public slots:
    void setAvailableDevs();
    void setDefaultDevInfo(QStringList currentDevice);
    void set_sbPacketInfo(int packetsReceived, int packetsDisplayed, int packetsDropped);

private slots:
    void on_actionClear_triggered();
    void on_actionQuit_triggered();

    void on_actionAbout_triggered();
    void on_actionAbout_Qt_triggered() const;
    void on_actionWebsite_triggered();

    void on_actionOptions_triggered();

    /* live capture controls */
    void on_actionStart_triggered();
    void on_actionStop_triggered();

    /* file action controls */
    void on_actionOpen_triggered();
    void on_actionOpenRecent_triggered();
    void on_actionSaveAs_triggered();
    void on_actionSave_triggered();

    /* packet movement controls */
    void on_actionNext_Packet_triggered();
    void on_actionPrevious_Packet_triggered();
    void on_actionFirst_Packet_triggered();
    void on_actionLast_Packet_triggered();

    /* filter controls */
    void on_actionShowFilter_triggered();
    void on_actionHideFilter_triggered();

    /* filter combobox */
    void on_filterComboBox_textActivated(const QString &arg1);
    void on_filterComboBox_editTextChanged(const QString &arg1) const;
    void on_applyFilter_clicked();
    void on_cancelFilter_clicked();

    void on_actionShowComment_triggered();
    void on_actionHideComment_triggered();

    /**
     * @brief Receives signals from widget_capture_screen class.
     *          These eare usually from errors from thread processes
     *          started from widget_cpature_screen
     *
     * @param state Enum (en::threadState) value
     */
    void captureScreenStatus(en::threadState state);

    void init();
    /* used to set isFileSaved flag when user enters new comments */
    void packetCommentInsert(int packetCommentIndex);

    void on_actionAbout_PcapPlusPlus_triggered();

signals:
    void stop_liveCapture();

protected:
    void createStatusBar() const;
    void closeEvent(QCloseEvent *event) override;

private:
    bool closeQuitCleanUp();
    Ui::MainWindow *ui;

    DialogCaptureOptions *captureOptionsDialog = nullptr;
    /* capture option session settings */
    QString autoCaptureFile;
    bool isAutoCaptureSave;
    // mirrored in captureSCreen
    bool isShowAnalysis;
    bool isShowComment;
    QString captureFilter;
    int stopAfterSeconds;
    int stopAfterKBytes;
    int stopAfterPackets;

    WidgetCaptureScreen *captureScreen = nullptr;

    QVector<QStringList> availableDevs;
    QStringList defaultDev;
    QStringList openDev;
    pcpp::PcapLiveDevice *captureDev;

    QString openfileName;
    QString savefileName;
    QString lastSavedFileFolder; // holds file path minus file name of last saved file
    QString defaultSaveFileName; // = "savefile";
    QString defaultSaveFolder;
    const QString SUPPORTED_FILE_TYPES = "Wireshark/.. (*.pcapng) ;; Wireshark/tcpdump/..-pcap (*.pcap)";
    const QVector<QStringList> SUPPORTED_FILE_TYPES_LIST = {{"Wireshark/.. (*.pcapng)", "pcapng", "false", "true"},
                                                            {"Compressed.. (*.pcapng)", "pcapng", "true", "true"},
                                                            {"Wireshark/tcpdump/..-pcap (*.pcap)", "pcap", "false", "false"}};
    const int defaultFileTypeIndex = 0;
    const int supportFileTypeIndex = 0;
    const int fileSuffixIndex = 1;
    const int fileCompressionIndex = 2;
    const int supportsComments = 2;
    int defaultCompressionFactor = 10;

    QString getSupportFileTypes(const bool withComment) const;
    QString getDefaultFileType() const;
    QString getDefaultFileSuffix() const;
    QString getSelectedFileTypeSuffix(const QString &selectedFileType) const;
    bool isSelectedFileTypeCompressed(const QString &selectedFileType) const;

    const QString SAVE_FILE_QUIT_QUERY = "Do you want to save the captured packets before quitting?\n\nYour captured packetes will be lost if you do not save them.";
    const QString SAVE_FILE_CLOSE_QUERY = "Do you want to save the captured packets before closing?\n\nYour captured packetes will be lost if you do not save them.";

    const QString ERROR_FILE_TYPE_1 = "The file ";
    const QString ERROR_FILE_TYPE_2 = " isn't a capture file in a format NetVis understands.";
    const QString ERROR_FILE_PATH_1 = "Cannot open ";
    const QString ERROR_FILE_PATH_2 = " for reading.";
    const QString ERROR_FILE_FILTER = "Unsupporting filter option: Cannot set filter for file reader";

    /*
    enum class defining button states (ie enable/disabled) for the equivalent
    current application state
    */
    enum class appState
    {
        btn_init,               // initial button states
        btn_start,              // after start capture
        btn_stop,               // after stop capture
        btn_openfile_loading,   // during file load
        btn_openfile_afterload, // after file load
        btn_close,              // after close
        btn_quit,               // before quit actions
        btn_changed_file,       // after loaded file changed in some way (eg filter applied)
        btn_file_saving,
        btn_file_saved,
        btn_close_cancel // button state after cancelling close action
        // to add after file changed setting

    };

    void setMenuButtons(appState btnState);
    void setPacketMovementCntl(bool isEnabled);
    void hidePacketMovementCntl();
    void toggleFilterButton();
    void toggleCommentButton();

    void saveFile(const QString &fileName, bool compression);
    void readAppSettings();
    void writeAppSettings() const;

    /* recent files variables */
    enum
    {
        MaxRecentFiles = 5
    };

    void setRecentFilesMenu();

    /* status bar live, nr packets & current device labels */
    QLabel *sb_CurrentSource;
    QLabel *sb_CurrentDevice;
    QLabel *sb_PacketInfo;

    /* filters */
    CaptureFilterBPF *filterBPF;

    // const int MaxRecentFilters = 25;

    QStringList readRecentFilters();

    DialogActiveDevice *activeDevDialog;
};

#endif // MAIN_WINDOW_H
