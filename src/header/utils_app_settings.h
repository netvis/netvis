#ifndef UTILS_APP_SETTINGS_H
#define UTILS_APP_SETTINGS_H

#include <QCoreApplication>
#include <QSettings>
#include <QDir>
#include <QDebug>

#include "utils_common.h"

/**
 * \namespace AppSetting
 * \brief Namespace for global application settings functions. Uses the QSettings class to abstract the
 * underlying o/s means of saving the users application settings (e.g Windows Registry,
 * Linux "home/.config/<application>/<app.conf> file)
 *
 */
namespace AppSetting

{
    /**
     * @brief Returns the applications saved geometry settings
     *
     * @return QByteArray appGeometry
     */
    QByteArray getGeometry();

    /**
     * @brief Save the applications current geometry settings
     * @param[in] appGeometry QByteArray array as returned by QWidget::saveGeometry()
     *
     * @param appGeometry
     */
    void setGeometry(const QByteArray &appGeometry);

    /**
     * @brief Returns the Default Save Folder value
     *
     * @return QString saved folder path, or if none a default home/<appname>/
     */
    QString getDefaultSaveFolder();

    /**
     * @brief Saves the folder path
     *
     * @param folderName a QString value
     */
    void setDefaultSaveFolder(const QString &folderName);

    /**
     * @brief Returns the default save filename for new packet details
     *
     * @return QString
     */
    QString getDefaultSaveFileName();

    /**
     * @brief Saves the default save filename for new packet details
     *
     * @param fileName QString containing valid folder path / filename
     */

    void setDefaultSaveFileName(const QString &fileName);

    /**
     * @brief Saves the default save file format for new packet details
     *
     * @param fileName QString containing valid file extension type
     */
    void setDefaultFileFormat(const cmm::fileFormat &fileFormat);

    /**
     * @brief Returns the default save fileformat for new packet details
     *
     * @return cmm::fileFormat enum
     */
    cmm::fileFormat getDefaultFileFormat();

    /**
     * @brief Saves the default save file compression on/off
     *
     * @param isCompression Bool true / false
     */
    void setDefaultFileCompression(const bool &isCompression);

    /**
     * @brief Returns the default save file compression on/off
     *
     * @return bool
     */
    bool getDefaultFileCompression();

    /**
     * @brief Returns folder path of the last packet file saved
     *
     * @return QString
     */
    QString getLastSaveFolder();

    /**
     * @brief Saves folder path of the last packet file saved
     *
     * @param folderName QString contain a valid folder path
     */
    void setLastSaveFolder(const QString &folderName);

    /**
     * @brief Returns the user application root folder
     *
     * @return QString
     */
    QString getApplicationRootFolder();

    /**
     * @brief Saves user application root folder
     *
     * @param folderName QString contain a valid folder path
     */
    void setApplicationRootFolder(const QString &folderName);

    /**
     * @brief Returns the default log filename and path
     *
     * @return QString
     */
    QString getLogFileName();

    /**
     * @brief Saves the default log filename and path
     *
     * @param fileName QString containing a absolute folder path/filename
     */
    void setLogFileName(const QString &fileName);

    /**
     * @brief Returns a list of recently opened/saved packet files
     *
     * @return QStringList
     */
    QStringList getRecentFiles();

    /**
     * @brief Saves a list of recently opened/saved packet files
     *
     * @param fileList QStringList
     */
    void setRecentFiles(const QStringList &fileList);

    /**
     * @brief Tests if the saved recent files list is emtpty
     *
     * @return true if there are recent files listed, otherwise false
     */
    bool isRecentFiles();

    /**
     * @brief Appends to the top of the recent files list and saves this
     *
     * @param fileName QString
     */
    void prependRecentFiles(const QString &fileName);

    /**
     * @brief Returns a list of recently opened/saved packet files
     *
     * @return QStringList
     */
    QStringList getRecentFilterList();

    /**
     * @brief Saves a list of recently used BPF filters sorted alphabetically
     *
     * @param filterList QStringList
     */
    void setRecentFilterList(const QStringList &filterList);

    /**
     * @brief Returns a count of the recently used BPF filters
     *
     * @return int
     */
    int isRecentFilter();

    /**
     * @brief Appends to the top of the recent filters list and saves this
     *
     * @param filter QString
     */
    void prependRecentFilter(const QString &filter);
}

#endif // UTILS_APP_SETTINGS