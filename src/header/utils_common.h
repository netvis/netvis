#ifndef UTILS_COMMON_H
#define UTILS_COMMON_H

// #include <QCoreApplication>
// #include <QDebug>
#include <QObject>
#include <QHash>
#include <QString>

class en : public QObject
{
public:
    enum class threadState
    {
        err_devOpen, // error opening device
        err_devNotFound,
        err_startLive,        // error starting live thread
        err_readerOpen,       // error opening pcap reader
        err_filter,           // error opening pcap reader
        err_pcapngReaderOpen, // error opening pcap reader
        err_pcapReaderOpen,   // error opening pcap reader
        err_pcapWriterOpen,   // error opening pcap reader
        err_pcapngWriterOpen, // error opening pcap reader
        err_writing,          // error opening pcap reader
        err_settingFilter,    // after close
        err_fileType,         // unrecognised file type
        ok_stopFile,          // stopped file load
        ok_stopLive,          // capture thread started
        // ok_stopSignal,        // live stop signal recv'd
        ok_startLive, // started live thread
    };
    Q_OBJECT
};

Q_DECLARE_METATYPE(en::threadState);

namespace cmm
{
    /*
       enum class defining thread states
       */
    enum class threadState
    {
        err_devOpen, // error opening device
        err_devNotFound,
        err_startLive,        // error starting live thread
        err_readerOpen,       // error opening pcap reader
        err_filter,           // error opening pcap reader
        err_pcapngReaderOpen, // error opening pcap reader
        err_pcapReaderOpen,   // error opening pcap reader
        err_pcapWriterOpen,   // error opening pcap reader
        err_pcapngWriterOpen, // error opening pcap reader
        err_writing,          // error opening pcap reader
        err_settingFilter,    // after close
        err_fileType,         // unrecognised file type
        ok_stopFile,          // stopped file load
        ok_stopLive,          // capture thread started
        // ok_stopSignal,        // live stop signal recv'd
        ok_startLive, // started live thread

    };

    enum fileFormat // making class enum causes link errors with Qhash!?
    {
        pcap = 0,
        pcapng = 1
    };

    const QHash<fileFormat, QString> fileExtensions =
        {
            {fileFormat::pcap, "pcap"},
            {fileFormat::pcapng, "pcapng"}};

    fileFormat getFileFormat(const QString &fileext);

    QString getFileExtension(cmm::fileFormat fileFormat);

}
#endif // UTILS_COMMON_H
