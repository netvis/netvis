#ifndef DIALOG_APP_ABOUT_H
#define DIALOG_APP_ABOUT_H

#include <QDialog>

namespace Ui
{
    class DialogAppAbout;
}

class DialogAppAbout : public QDialog
{
    Q_OBJECT

public:
    explicit DialogAppAbout(QWidget *parent = nullptr);
    ~DialogAppAbout() final;

private slots:
    void on_buttonBox_accepted();

private:
    Ui::DialogAppAbout *ui;
};

#endif // DIALOG_APP_ABOUT_H