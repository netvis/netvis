#ifndef WIDGET_CAPTURE_SCREEN_H
#define WIDGET_CAPTURE_SCREEN_H

#include <vector>

#include <QPlainTextEdit>
#include <QStandardItemModel>
#include <QSortFilterProxyModel>
#include <QTableView>
#include <QTime>
#include <QList>
#include <QWidget>
#include <QApplication>

#include "capture_packet_thread.h"
#include "capture_analysis_packet_handshakes.h"
#include "capture_analyse_handshakes.h"
#include "widget_packet_arrow.h"
#include "capture_filter_BPF.h"
#include "utils_common.h"

#include "packet_layer_link.h"
#include "packet_layer_internet.h"
#include "packet_layer_transport.h"
#include "packet_layer_presentation.h"
#include "packet_layer_application.h"

#include <PcapFileDevice.h>

/*
    enum class defining navigation commands
    */
enum class arrowMove
{
    first,
    last,
    previous,
    next
};

enum class arrowDisplay
{
    packetDefault,
    packetAnalysis,
    packetComments,
    packetAll
};

namespace Ui
{
    class WidgetCaptureScreen;
}

class WidgetCaptureScreen : public QWidget
{
    Q_OBJECT

public:
    explicit WidgetCaptureScreen(QStringList defaultDev,
                                 QStatusBar *statusBar,
                                 QWidget *parent = nullptr);

    ~WidgetCaptureScreen() final;

    /* session capture options */
    int stopAfterSeconds;
    int stopAfterKBytes;
    int stopAfterPackets;
    QString captureFilter;
    bool isShowAnalysis;
    bool isShowComment;

    /* Opens a live capture feed */
    void prepareOpenCaptureLive(const QString &capturefilter = nullptr);
    void stopLiveCapture();

    /* Opens a pcap/pcapng file */
    void prepareOpenCaptureFile(const QString &fileName, const QString &capturefilter = nullptr);
    void stopFileLoading();

    /* Saves packets to file */
    bool prepareSaveCaptureFile(const QString &fileName, bool compression) const;

    /* refreshes arrow diagram with filtered packets */
    void setDisplayFilter(const QString &filter);

    /* clears display filter and calls refreshPacketDisplay */
    void clearDisplayFilter();
    void closeDiagram();

    void setPacketDisplayFormat(arrowDisplay display) const;

signals:
    /* signal for new packet arrival */
    void packetInfo(int packetsReceived, int packetsDisplayed, int packetsDropped);
    /* signal that user comments inserted */
    void packetCommentInsert(int packetCommentIndex);
    /* signal capturethread status to mainwindow */
    void emitThreadStatus(en::threadState state);

public slots:
    void renderDiagram(int packetNumber);
    void threadStatus(en::threadState state);
    void onArrowSelect(int packetNumber);
    void onArrowMove(arrowMove move);

private:
    /* initialise capture screen settings */
    void init();

    /* clear the currently displayed packet information */
    void clearDiagram();

    // int analysePacket(pcpp::Packet *parsedPacket, void *arrowCookie, void *infoCookie);
    int rendorArrowWidget(const pcpp::Packet *packet, int packetNumber, bool display);

    void checkOpeningFileDev();
    void getPacketArrowAddress(const pcpp::Packet &packet, WidgetPacketArrow *arrow) const;
    int rendorPacketInfoWidget(const pcpp::Packet *packet);
    int rendorPacketHexWidget(const pcpp::Packet *packet);
    void setUnknownProtocol(int lastLayer, WidgetPacketArrow *arrow) const;
    void setAnalysisComment(int packetIndex, const std::string &analysisComment) const;

    // void selectedPacket(int selection);
    bool setSelectArrow(int arrowIndex, bool isSelected);
    void switchPacketFocus(int newPacketIndex);
    void displayArrow(int arrowIndex, bool isDisplay) const;

    CaptureAnalyseHandshakes *analyseHandshakes;
    Ui::WidgetCaptureScreen *ui;
    QStatusBar *statusBar;
    QTreeWidget *packetLayerInfo;
    QPlainTextEdit *rawPacketData;

    QFrame *inner;
    QVBoxLayout *packetLayout;

    CapturePacketThread *capturePacketThread;
    WidgetPacketArrow *arrow;

    /*
    List of all captured/loaded packets. Do not iterate
    over this container in the main thread as it could be accessed
    by the seperate live capture thread
    */
    pcpp::RawPacketVector rawPacketList;

    /* last packet number captured (0 based) */
    int lastPacketNumberIndex;

    /* packet number of currently selected arrow(0 based) */
    int packetNumberFocusIndex;

    /*
    List of bools indicating if corresponding packet nr is displayed.
    Do not iterate over this container in the main thread as it could
    be accessed by the seperate live capture thread. Use lastPacketNumberIndex
    for iteration.
     */
    QVector<bool> display_hidePacketsList;

    /*
    List of packet comments. Do not iterate
    over this container in the main thread as it could be accessed
    by the seperate live capture thread. Use lastPacketNumberIndex
    for iteration.
    */
    QVector<QString> packetCommentList;

    /*
    List of packet analysis info. Do not iterate
    over this container in the main thread as it could be accessed
    by the seperate live capture thread. Use lastPacketNumberIndex
    for iteration.
    */
    // QVector<QString> packetAnalysisList;

    QStringList defaultDev;
    std::string defaultDevName;

    /* app/data state flags */
    bool isLiveCapture;
    bool isFileLoading;
    bool isFileComment = false;
    bool isDisplayFilter;

    /* session capture counters */
    int packetsCaptured;
    int secondsCaptured;
    long bytesCaptured;

    int displayedPacketCnt;
    int droppedPacketCnt;
    // int display;

    // pcpp::BpfFilterWrapper *filter;

    /* filters */
    CaptureFilterBPF *filterBPF;
    QString displayfilter;

    int error_message;
};

#endif // WIDGET_CAPTURE_SCREEN_H
