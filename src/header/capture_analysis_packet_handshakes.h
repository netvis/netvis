#ifndef CAPTURE_ANALYSIS_PACKET_HANDSHAKES_H
#define CAPTURE_ANALYSIS_PACKET_HANDSHAKES_H

#include <iostream>

#include <QDebug>
#include <QThread>
#include <QLayout>

#include <PcapLiveDeviceList.h>
#include <RawPacket.h>
#include <Packet.h>

#include <SSLLayer.h>
#include <TcpLayer.h>

#include "widget_packet_arrow.h"

class CaptureAnalysisPacketHandshakes : public QObject
{
    Q_OBJECT
public:
    explicit CaptureAnalysisPacketHandshakes(const pcpp::Packet *packet);
    ~CaptureAnalysisPacketHandshakes() final;

public slots:
    void process();

signals:
    void newAnalysis(std::string analysis);
    void finished();

private:
    void analyseTCPThreeWayHandshake() const;
    void analyseTLSHandshake() const;

    void setPacketAnalysis(std::string analysis) const;

    const pcpp::Packet *packet;
};

#endif //  CAPTURE_ANALYSIS_PACKET_HANDSHAKES_H