#ifndef DIALOG_RECENT_FILES_H
#define DIALOG_RECENT_FILES_H

#include "qlistwidget.h"
#include <QDialog>
#include <QDir>
#include <QSettings>
#include <QDebug>
#include "utils_app_settings.h"

namespace Ui
{
    class DialogRecentFiles;
}

class DialogRecentFiles final : public QDialog
{
    Q_OBJECT

public:
    explicit DialogRecentFiles(QWidget *parent = nullptr);
    ~DialogRecentFiles() final;
    QString getFile() const;

private slots:
    void on_buttonBox_accepted();

    void on_clearList_clicked(bool checked);

    void on_recentFilesList_itemDoubleClicked(QListWidgetItem const *item);

    void on_buttonBox_rejected();

    void on_recentFilesList_itemClicked(QListWidgetItem const *item);

private:
    Ui::DialogRecentFiles *ui;

    QString selectedFile;
    enum
    {
        MaxRecentFiles = 20
    };
    const QString recentFileListGroupKey = "recentFileList";
    const QString recentFilesKey = "file";
    void readRecentFiles() const;
};
#endif // DIALOG_RECENT_FILES_H
