#include <stdio.h>
#include "main_window.h"
#include "app_version.h"
#include "git_build.h"
#include "utils_logger.h"
#include "utils_app_settings.h"

#include <QApplication>
#include <QCommandLineParser>
#include <QCommandLineOption>
#include <QDebug>

int main(int argc, char *argv[])
{
    // handle command line options
    QApplication app(argc, argv);

    QCoreApplication::setOrganizationName(ORG_NAME);
    QCoreApplication::setOrganizationDomain(ORG_DOMAIN);
    QCoreApplication::setApplicationName(APP_NAME);
    QCoreApplication::setApplicationVersion(APP_VERSION);

    QCommandLineParser parser;
    parser.setApplicationDescription(QCoreApplication::applicationName());
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addPositionalArgument("file", "The file to open, either <filename>.pcap or <filename>.pcapng");
    parser.process(app);

    // qDebug() << "App name: " << QCoreApplication::applicationName();
    // qDebug() << "Home dir: " << QDir::homePath();
    // qDebug() << "Current dir: " << QDir::currentPath();
    QString defaultAppFolder = AppSetting::getApplicationRootFolder();

    // qDebug() << "settings log name:" << AppSetting::getLogFileName();

    // QString logFile = QDir::currentPath() + "/" +
    //                   QCoreApplication::applicationName() + "/" +
    //                   QCoreApplication::applicationName() + ".log";
    QString logFile = AppSetting::getLogFileName();
    // qDebug() << "app folder: " << defaultAppFolder;
    // qDebug() << "log file: " << AppSetting::getLogFileName();

    /* check or initialise default app folder */
    if (!QDir(defaultAppFolder).exists())
    {
        QDir().mkdir(defaultAppFolder);
    }
    else
    {
        /* clear old log file if exists */
        QDir().remove(logFile);
    }

    /* install app logger */
    qInstallMessageHandler(Logger::messageHandler);

    QString msg = QString::fromStdString("Starting " + std::string(APP_NAME) +
                                         " v" + std::string(APP_VERSION) +
                                         " (Build " + std::string(GIT_REV) + "-" +
                                         std::string(GIT_BRANCH) + " " +
                                         std::string(GIT_TAG) + ":" +
                                         std::string(APP_BUILD_DATE));

    qInfo() << msg;
    qInfo() << "O/s: " << QSysInfo::prettyProductName();
    qInfo() << "Hw platform: " << QSysInfo::currentCpuArchitecture();
    qInfo() << "Log file: " << logFile;
    qInfo() << "Home dir: " << QDir::homePath();

    /* create the main window */
    MainWindow w;
    w.setWindowTitle(APP_NAME);

    /* check for command line arguments */
    if (!parser.positionalArguments().isEmpty())
        w.loadFile(parser.positionalArguments().first());
    w.show();

    return QApplication::exec();
}
