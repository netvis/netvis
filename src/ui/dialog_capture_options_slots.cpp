#include "dialog_capture_options.h"
#include "ui_dialog_capture_options.h"

void DialogCaptureOptions::on_startCaptureBtn_clicked()
{
    /* starts capture on selected device */
}

void DialogCaptureOptions::on_closeDialogBtn_clicked()
{
    /* closes dialog box */
    QDialog::close();
}

void DialogCaptureOptions::on_startCaptureBtn_clicked(bool checked)
{
    if (checked)
        startLiveCapture = true;
    else
        startLiveCapture = false;
}

void DialogCaptureOptions::on_commentCheck_clicked(bool checked)
{
    if (checked)
        showComment = true;
    else
        showComment = false;
}

void DialogCaptureOptions::on_analysisCheck_clicked(bool checked)
{
    if (checked)
        showAnalysis = true;
    else
        showAnalysis = false;
}

void DialogCaptureOptions::on_selectPcapNg_clicked(bool checked) const
{
    if (checked)
        AppSetting::setDefaultFileFormat(cmm::pcapng);
    else
        AppSetting::setDefaultFileFormat(cmm::pcap);
}

void DialogCaptureOptions::on_selectPcap_clicked(bool checked) const
{
    if (checked)
        AppSetting::setDefaultFileFormat(cmm::pcap);
    else
        AppSetting::setDefaultFileFormat(cmm::pcapng);
}

void DialogCaptureOptions::on_autoNewFile_clicked(bool checked)
{
    if (checked)
        autoNewFile = true;
    else
        autoNewFile = false;
}

void DialogCaptureOptions::on_afterPackets_clicked(bool checked)
{
    if (!checked)
    {
        afterPacketCnt = 0;
        ui->spinPacketsAfter->setValue(0);
    }
}

void DialogCaptureOptions::on_afterSeconds_clicked(bool checked)
{
    if (!checked)
    {
        afterSeconds = 0;
        ui->spinSecondsAfter->setValue(0);
    }
}

void DialogCaptureOptions::on_afterFileSize_clicked(bool checked)
{
    if (!checked)
    {
        afterKiloBytes = 0;
        ui->spinFileSizeAfter->setValue(0);
    }
}

void DialogCaptureOptions::on_spinPacketsAfter_valueChanged(int arg1)
{
    afterPacketCnt = arg1;
}

void DialogCaptureOptions::on_spinSecondsAfter_valueChanged(int arg1)
{
    afterSeconds = arg1;
}

void DialogCaptureOptions::on_spinFileSizeAfter_valueChanged(int arg1)
{
    afterKiloBytes = arg1;
}

void DialogCaptureOptions::on_browseFolder_clicked()
{
}

void DialogCaptureOptions::on_autoCaptureFile_editingFinished()
{
    autoCaptureFile = ui->autoCaptureFile->text();
}
void DialogCaptureOptions::on_treeWidget_itemActivated(QTreeWidgetItem *item, int column)
{
    // devItem = new QTreeWidgetItem(ui->treeWidget);
    // devItem->setText(0, device.at(0));
    if (item != nullptr)
    {
        QString devName = item->text(0);
        /* get the devlist for this name */
    }
}

void DialogCaptureOptions::on_compressionBox_clicked(bool checked) const
{
    if (checked)
        AppSetting::setDefaultFileCompression(true);
    else
        AppSetting::setDefaultFileCompression(false);
}

void DialogCaptureOptions::on_cancelFilter_clicked()
{
    /* get current filter string */
    QString filter = ui->filterComboBox->currentText().trimmed();
    if (filterBPF->setFilter(filter))
    {
        /* force display of combo placeholdertext */
        ui->filterComboBox->setCurrentIndex(-1);

        /* clear displayfilter & reset controls */
        ui->filterComboBox->clearEditText();
        ui->filterComboBox->lineEdit()->setStyleSheet("QLineEdit"
                                                      "{"
                                                      "background : white;"
                                                      "}");
    }
    ui->applyFilter->setEnabled(true);
    ui->cancelFilter->setDisabled(true);
    ui->filterComboBox->setEnabled(true);
}

void DialogCaptureOptions::on_applyFilter_clicked()
{
    /* get current filter string */
    QString filter = ui->filterComboBox->currentText().trimmed();

    /* ignore empty/all spaces/nullptr filter and if not valid */
    if (filterBPF->setFilter(filter))
    {
        /* insert filter at top of list*/
        ui->filterComboBox->insertItem(0, filter);
        /* update recentFilters list */
        AppSetting::prependRecentFilter(filter);
        /* disable applyFilter button/enable cancelFilter */
        ui->applyFilter->setDisabled(true);
        ui->cancelFilter->setEnabled(true);
        ui->filterComboBox->setEnabled(false);
        ui->filterComboBox->lineEdit()->setStyleSheet("QLineEdit"
                                                      "{"
                                                      "background : white"
                                                      "}");
    }
    else
    {
        /* highlight BPF syntax error*/
        ui->filterComboBox->lineEdit()->setStyleSheet("QLineEdit"
                                                      "{"
                                                      "background : red"
                                                      "}");
        ui->applyFilter->setDisabled(true);
        ui->cancelFilter->setEnabled(true);
    }
}

void DialogCaptureOptions::on_filterComboBox_editTextChanged(const QString &arg1) const
{
    ui->filterComboBox->lineEdit()
        ->setStyleSheet("QLineEdit"
                        "{"
                        "background : white"
                        "}");
}

void DialogCaptureOptions::on_filterComboBox_textActivated(const QString &arg1)
{
    QString filterText = arg1;
    /* test filterstring is valid BPF syntax */
    if (!filterBPF->setFilter(filterText.trimmed()))
    {
        /* delete this entry inserted by combo cntrl from list */
        ui->filterComboBox->removeItem(ui->filterComboBox->findText(filterText));
        ui->filterComboBox->setCurrentText(filterText);
        ui->filterComboBox->lineEdit()
            ->setStyleSheet("QLineEdit"
                            "{"
                            "background : pink"
                            "}");
        ui->cancelFilter->setEnabled(true);
        ui->applyFilter->setDisabled(true);
    }
    else
    {
        /* get current filter string */
        QString filter = filterBPF->getFilter();

        /* update recentFilters list */
        AppSetting::prependRecentFilter(filter);
    }
}
