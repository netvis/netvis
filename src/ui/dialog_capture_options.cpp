#include "dialog_capture_options.h"
#include "./ui_dialog_capture_options.h"

// DialogCaptureOptions::DialogCaptureOptions(QVector<QStringList> availableDevs,
//                                            QStringList defaultDev, QWidget *parent)
//     : QDialog(parent),
//       ui(new Ui::DialogCaptureOptions)
DialogCaptureOptions::DialogCaptureOptions(QWidget *parent)
    : QDialog(parent),
      ui(new Ui::DialogCaptureOptions)
{
    ui->setupUi(this);
    setDialogInputTab(availableDevs);

    init();
}

DialogCaptureOptions::~DialogCaptureOptions()
{
    delete ui;
}

void DialogCaptureOptions::init()
{
    /* set first tab on startup */
    ui->tabWidget->setCurrentIndex(0);

    /* setup controls */
    startLiveCapture = false;

    /* add statusbar for tooltips/messages */

    setStatusTip("");
    /* setup filtercomb box */
    filterBPF = new CaptureFilterBPF();
    /* set filter bar buttons state */
    ui->cancelFilter->setDisabled(true);
    ui->applyFilter->setEnabled(true);

    /* populate combo with filter list */
    if (AppSetting::isRecentFilter())
    {
        /* clear old list */
        ui->filterComboBox->clear();
        /* sort filters case insensitive */
        QStringList filterList = readRecentFilters();
        filterList.sort(Qt::CaseInsensitive);
        /* add current list */
        ui->filterComboBox->addItems(filterList);
    }
    /* force display of combo placeholdertext */
    ui->filterComboBox->setCurrentIndex(-1);
    // this line supported from QT v5.15
    ui->filterComboBox->lineEdit()->setPlaceholderText(tr("Enter a BPF display filter.."));
}

void DialogCaptureOptions::showStatusTip(const QString &statusTip)
{
    ui->statusTip->setText(statusTip);
}

void DialogCaptureOptions::setDialogInputTab(QVector<QStringList> availableDevs)
{
    for (const auto &device : availableDevs)
    {
        /* adds interface to input tab */
        devItem = new QTreeWidgetItem(ui->treeWidget);
        devItem->setText(0, device.at(0));

        /* adds address child to interface */
        devChildIPv4 = new QTreeWidgetItem();
        devChildIPv4->setText(0, QString("IPv4 address: ") + device.at(1));
        devItem->addChild(devChildIPv4);

        devChildIPv6 = new QTreeWidgetItem();
        devChildIPv6->setText(0, QString("IPv6 address: ") + device.at(4));
        devItem->addChild(devChildIPv4);

        devChildGateway = new QTreeWidgetItem();
        devChildGateway->setText(0, QString("Gateway: ") + device.at(2));
        devItem->addChild(devChildGateway);

        devItem->setText(1, device.at(3));
    }

    return;
}
QStringList DialogCaptureOptions::readRecentFilters()
{
    QStringList filterList = AppSetting::getRecentFilterList();
    QStringList cleanFilterList;
    for (int i = 0; i < filterList.size(); ++i)
    {
        /* if filter valid syntax accept it */
        if (filterBPF->setFilter(filterList.at(i)))
        {
            // qDebug() << "Reading filter: " << filterList.at(i);
            cleanFilterList.append(filterList.at(i));
        }
    }

    return cleanFilterList;
}
