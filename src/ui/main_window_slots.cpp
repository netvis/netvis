#include "main_window.h"
#include "ui_main_window.h"

void MainWindow::on_actionOpen_triggered()
{
    // get last used savePath or default to users homepath
    QString filepath;
    if (lastSavedFileFolder.isEmpty())
        filepath = defaultSaveFolder;
    else
        filepath = lastSavedFileFolder;

    /* opening a file and returning the file path */
    openfileName = QFileDialog::getOpenFileName(this,
                                                "Open Capture File",
                                                filepath, // QDir::homePath(),
                                                getSupportFileTypes(isFileComment));

    /* if file name not empty load packet data */
    if (!openfileName.isEmpty())
    {
        /* clear any previous content */
        ui->actionClear->trigger();
        loadFile(openfileName);
    }
    return;
}

void MainWindow::on_actionOpenRecent_triggered()
{
    auto recentFilesDialog = new DialogRecentFiles();
    if (recentFilesDialog->exec() == QDialog::Accepted)
    {
        QString load_FileName = recentFilesDialog->getFile();
        loadFile(load_FileName);
    }
}

void MainWindow::loadFile(const QString load_fileName)
{
    if (captureFilter != nullptr && !captureFilter.isEmpty())
        sb_CurrentSource->setText(tr("Loading file (filtered): ") + load_fileName);
    else
        sb_CurrentSource->setText(tr("Loading file: ") + load_fileName);

    /* set menu/toolbar button states during load */
    setMenuButtons(appState::btn_openfile_loading);

    // show packet info on statusbar
    show_sbPacketInfo();

    /* update status bar file details */
    set_sbCurrentSource(load_fileName, true);

    /* add this file to recent files list */
    AppSetting::prependRecentFiles(load_fileName);

    /* user input active device details */
    activeDevDialog = new DialogActiveDevice(this);
    activeDevDialog->init(defaultDev);
    connect(activeDevDialog, &DialogActiveDevice::updateDevList,
            this, &MainWindow::setDefaultDevInfo);
    activeDevDialog->exec();

    if (captureScreen == nullptr)
    {
        captureScreen = new WidgetCaptureScreen(defaultDev, ui->statusbar);
        connect(captureScreen, &WidgetCaptureScreen::packetInfo,
                this, &MainWindow::set_sbPacketInfo);
        connect(captureScreen, &WidgetCaptureScreen::emitThreadStatus,
                this, &MainWindow::captureScreenStatus);

        ui->mainLayout->addWidget(captureScreen);
        /* ensure capture display area visible */
        captureScreen->setVisible(true);
        ui->dummyScrollArea->setHidden(true);
    }
    else
    {
        /* clear any previous content */
        ui->actionClear->trigger();
        /* ensure the capturescreen is visible after any clear diagram call */
        captureScreen->setVisible(true);
        ui->dummyScrollArea->setHidden(true);
    }
    /* initialise session capture settings */
    captureScreen->isShowAnalysis = isShowAnalysis;
    captureScreen->isShowComment = isShowComment;
    captureScreen->stopAfterKBytes = stopAfterKBytes;
    captureScreen->stopAfterSeconds = stopAfterSeconds;
    captureScreen->stopAfterPackets = stopAfterPackets;

    isFileOpen = true;
    isFileSaved = true; // set true to indicate no need to save on exit
    isFileLoading = true;
    captureScreen->prepareOpenCaptureFile(load_fileName, captureFilter);
    isFileLoading = false;

    /* set menu/toolbar button states post load */
    setMenuButtons(appState::btn_openfile_afterload);

    sb_CurrentSource->setText(tr("File: ") + load_fileName);
}

void MainWindow::on_actionSave_triggered()
{
    // get last used savePath or default to users homepath
    QString filepath;
    if (lastSavedFileFolder.isEmpty())
        filepath = defaultSaveFolder;
    else
        filepath = lastSavedFileFolder;

    /* create default savefile path */
    QString saveFilePath = filepath + "/" + defaultSaveFileName + "." + getDefaultFileSuffix();

    if (!isFileSaved)
    {
        saveFile(saveFilePath, false);
    }
}

void MainWindow::on_actionSaveAs_triggered()
{
    /* get last used savePath or default to users homepath */

    QString filepath;
    if (lastSavedFileFolder.isEmpty())
        filepath = defaultSaveFolder;
    else
        filepath = lastSavedFileFolder;

    /* set default savefile path */
    filepath += "/" + defaultSaveFileName;

    /* set default file type for save */
    QString selectedFilter = getDefaultFileType();
    /* opening a file and returning the file path */
    savefileName = QFileDialog::getSaveFileName(this,
                                                "Save Capture File",
                                                filepath,
                                                getSupportFileTypes(isFileComment),
                                                &selectedFilter);

    if (!savefileName.isEmpty())
    {
        QString fileExtension = getSelectedFileTypeSuffix(selectedFilter);
        /* check if suffix present if not add it to savefilename */
        if (QFileInfo(savefileName).suffix() == nullptr)
        {
            savefileName += "." + fileExtension;
        }

        /*
            save path to directory where last file saved - use as default
            for open and save
        */
        QDir d = QFileInfo(savefileName).absoluteDir();
        lastSavedFileFolder = d.absolutePath();

        saveFile(savefileName,
                 isSelectedFileTypeCompressed(selectedFilter));
    }

    return;
}

void MainWindow::saveFile(const QString &fileName, bool compression)
{
    sb_CurrentSource->setText("Saving to file :" + fileName);

    isFileSaving = true;
    /* set menu/toolbar button states during save */
    setMenuButtons(appState::btn_file_saving);

    if (captureScreen->prepareSaveCaptureFile(fileName, compression))
    {
        sb_CurrentSource->setText(tr("File saved :") + fileName);
        qInfo() << "Capture file successfully saved: " + fileName;
    }
    else
    {
        sb_CurrentSource->setText(tr("Error saving to file :") + fileName);
        qInfo() << "Error saving file: " + fileName;
    }

    isFileSaving = false;
    isFileSaved = true; // set true to indicate no need to save on exit

    /* add to recent files list */
    AppSetting::prependRecentFiles(fileName);

    /* set menu/toolbar button states after save */
    setMenuButtons(appState::btn_file_saved);
}

/* receives signal if a user comment has been added or -1 if screen cleared */
void MainWindow::packetCommentInsert(int packetCommentIndex)
{
    if (packetCommentIndex == -1)
    {
        isFileComment = false;
    }
    else
    {
        isFileComment = true;
    }
}

/*
    Close menu action:  closes any live capture session, and prompts user to save
    captured packets to file and/or save file loaded and subsequently modified eg filtered
    - so user can save filtered content to new file.  After which application set to its
    initial starting state.
*/
void MainWindow::on_actionClear_triggered()
{
    /* set menu/toolbar button states during close */
    setMenuButtons(appState::btn_close);
    sb_CurrentSource->setText(tr("Closing capture session..."));
    if (!closeQuitCleanUp())
    {
        /* user aborted close, return to initial state*/
        setMenuButtons(appState::btn_init);
        return;
    }

    /* delete the capture screen (and capture thread/rawpacklist) */
    if (captureScreen != nullptr)
    {
        /* hide the capture screen while clearing */
        captureScreen->setHidden(true);
        captureScreen->closeDiagram();
        ui->dummyScrollArea->setVisible(true);
    }
    /* packets either saved or discarded */
    isFileSaved = true;
    /* clear packet stats info */
    set_sbPacketInfo(0, 0, 0);
    // set menu/toolbar button states during close
    setMenuButtons(appState::btn_init);
}

void MainWindow::on_actionStart_triggered()
{
    if (captureFilter != nullptr && !captureFilter.isEmpty())
        sb_CurrentSource->setText(tr("Starting live capture (filtered): "));
    else
        sb_CurrentSource->setText(tr("Starting live capture: "));

    // sb_CurrentSource->setText(tr("Starting live capture "));
    //  set menu/toolbar button states during live capture
    setMenuButtons(appState::btn_start);

    // show packet info on statusbar
    show_sbPacketInfo();

    if (captureScreen == nullptr)
    {
        captureScreen = new WidgetCaptureScreen(defaultDev, ui->statusbar);
        connect(captureScreen, &WidgetCaptureScreen::packetInfo,
                this, &MainWindow::set_sbPacketInfo);
        connect(captureScreen, &WidgetCaptureScreen::emitThreadStatus,
                this, &MainWindow::captureScreenStatus);

        ui->mainLayout->addWidget(captureScreen);
        /* ensure capture display area visible */
        captureScreen->setVisible(true);
        ui->dummyScrollArea->setHidden(true);
    }
    else
    {
        /* clear any previous content */
        ui->actionClear->trigger();
        /* ensure the capturescreen is visible after any cleardiagram call */
        captureScreen->setVisible(true);
        ui->dummyScrollArea->setHidden(true);
    }
    /* initialise session capture settings */
    captureScreen->isShowAnalysis = isShowAnalysis;
    captureScreen->isShowComment = isShowComment;
    captureScreen->stopAfterKBytes = stopAfterKBytes;
    captureScreen->stopAfterSeconds = stopAfterSeconds;
    captureScreen->stopAfterPackets = stopAfterPackets;
    captureScreen->captureFilter = captureFilter;

    isLiveCapture = true;
    isFileSaved = false;
    captureScreen->prepareOpenCaptureLive();
}

void MainWindow::on_actionStop_triggered()
{
    if (isLiveCapture)
    {
        sb_CurrentSource->setText(tr("Stopping live capture..."));

        /* stops live capture session */
        captureScreen->stopLiveCapture();
    }
    else if (isFileLoading)
    {
        sb_CurrentSource->setText(tr("Stopping file load..."));

        /* stops live capture session */
        captureScreen->stopLiveCapture();
    }

    isLiveCapture = false;
    isFileSaved = false;
    // sb_CurrentSource->setText(tr("Live capture stopped"));

    /* set menu/toolbar button states after stop capture */
    setMenuButtons(appState::btn_stop);
}

void MainWindow::on_actionQuit_triggered()
{
    /* set menu/toolbar button states during quit action */
    setMenuButtons(appState::btn_quit);
    sb_CurrentSource->setText(tr("Quitting capture session..."));

    if (!closeQuitCleanUp())
    {
        /* user aborted quit, return to initial state*/
        setMenuButtons(appState::btn_init);
        return;
    }

    /* delete the capture screen (and capture thread/rawpacklist) */
    if (captureScreen != nullptr)
    {
        /* hide the capture screen while clearing */
        captureScreen->setHidden(true);
        captureScreen->closeDiagram();
        ui->dummyScrollArea->setVisible(true);
    }
    close();
}

bool MainWindow::closeQuitCleanUp()
{
    /* stop any current file loading or live capture session */
    if (isFileLoading)
    {
        captureScreen->stopFileLoading();
    }
    else if (isLiveCapture)
    {
        captureScreen->stopLiveCapture();
    }

    /* check if unsaved packet data */
    if (!isFileSaved)
    {
        QMessageBox saveQuery(QMessageBox::Warning,
                              tr("Do you want to save the current packets?"),
                              SAVE_FILE_QUIT_QUERY,
                              QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel,
                              this);

        saveQuery.setDefaultButton(QMessageBox::Cancel);
        int action = saveQuery.exec();
        if (action == QMessageBox::Save)
        {
            // The user wants to save the current capture
            ui->actionSave->trigger();
            return true; // continue with close/quit action
        }
        else if (action == QMessageBox::Cancel)
        {
            /* set menu/toolbar button states to init state */
            setMenuButtons(appState::btn_init);
            return false;
        }
    }
    return true; // continue with close/quit action
}

void MainWindow::on_actionNext_Packet_triggered()
{
    captureScreen->onArrowMove(arrowMove::next);
}

void MainWindow::on_actionPrevious_Packet_triggered()
{
    captureScreen->onArrowMove(arrowMove::previous);
}

void MainWindow::on_actionFirst_Packet_triggered()
{
    captureScreen->onArrowMove(arrowMove::first);
}

void MainWindow::on_actionLast_Packet_triggered()
{
    captureScreen->onArrowMove(arrowMove::last);
}

void MainWindow::on_actionShowFilter_triggered()
{
    /* hide show filter / make visbile close filter */
    toggleFilterButton();

    /* clear any previous filter */
    ui->filterComboBox->clearEditText();
    // set lineedit background to default

    /* set filter bar buttons state */
    ui->cancelFilter->setDisabled(true);
    ui->applyFilter->setEnabled(true);

    /* populate combo with filter list */
    if (AppSetting::isRecentFilter())
    {
        /* clear old list */
        ui->filterComboBox->clear();
        /* sort filters case insensitive */
        QStringList filterList = readRecentFilters();
        filterList.sort(Qt::CaseInsensitive);
        /* add current list */
        ui->filterComboBox->addItems(filterList);
    }
    /* force display of combo placeholdertext */
    ui->filterComboBox->setCurrentIndex(-1);
    // this line supported from QT v5.15
    ui->filterComboBox->lineEdit()->setPlaceholderText(tr("Enter a BPF display filter.."));
}

/* hides filter bar which is initialise on next showfilter */
void MainWindow::on_actionHideFilter_triggered()
{
    toggleFilterButton();
}

void MainWindow::on_filterComboBox_textActivated(const QString &arg1)
{
    QString filterText = arg1;
    /* test filterstring is valid BPF syntax */
    if (!filterBPF->setFilter(filterText.trimmed()))
    {
        /* delete this entry inserted by combo cntrl from list */
        ui->filterComboBox->removeItem(ui->filterComboBox->findText(filterText));
        ui->filterComboBox->setCurrentText(filterText);
        ui->filterComboBox->lineEdit()
            ->setStyleSheet("QLineEdit"
                            "{"
                            "background : pink"
                            "}");
        ui->cancelFilter->setEnabled(true);
        ui->applyFilter->setDisabled(true);
    }
    else
    {
        /* get current filter string */
        QString filter = filterBPF->getFilter();

        /* update recentFilters list */
        AppSetting::prependRecentFilter(filter);
    }
}

/* reset combo box lineedit controls background to default */
void MainWindow::on_filterComboBox_editTextChanged(const QString &arg1) const
{
    ui->filterComboBox->lineEdit()->setStyleSheet("QLineEdit"
                                                  "{"
                                                  "background : white"
                                                  "}");
}

/*  action applyfilter */
void MainWindow::on_applyFilter_clicked()
{
    /* get current filter string */
    QString filter = ui->filterComboBox->currentText().trimmed();

    /* ignore empty/all spaces/nullptr filter and if not valid */
    if (filterBPF->setFilter(filter))
    {
        /* insert filter at top of list*/
        ui->filterComboBox->insertItem(0, filter);
        /* update recentFilters list */
        AppSetting::prependRecentFilter(filter);
        /* disable applyFilter button/enable cancelFilter */
        ui->applyFilter->setDisabled(true);
        ui->cancelFilter->setEnabled(true);
        ui->filterComboBox->setEnabled(false);
        ui->filterComboBox->lineEdit()->setStyleSheet("QLineEdit"
                                                      "{"
                                                      "background : white"
                                                      "}");
        /* refresh display with filter */
        captureScreen->setDisplayFilter(filter);
    }
    else
    {
        /* highlight BPF syntax error*/
        ui->filterComboBox->lineEdit()->setStyleSheet("QLineEdit"
                                                      "{"
                                                      "background : red"
                                                      "}");
        ui->applyFilter->setDisabled(true);
        ui->cancelFilter->setEnabled(true);
    }
}

/*
    cancels display filter / calls diagram refresh if
    previous valid filter
*/
void MainWindow::on_cancelFilter_clicked()
{
    /* get current filter string */
    QString filter = ui->filterComboBox->currentText().trimmed();
    if (filterBPF->setFilter(filter))
    {
        /* force display of combo placeholdertext */
        ui->filterComboBox->setCurrentIndex(-1);

        /* clear displayfilter & reset controls */
        captureScreen->clearDisplayFilter();
        ui->filterComboBox->clearEditText();
        ui->filterComboBox->lineEdit()->setStyleSheet("QLineEdit"
                                                      "{"
                                                      "background : white;"
                                                      "}");
    }
    ui->applyFilter->setEnabled(true);
    ui->cancelFilter->setDisabled(true);
    ui->filterComboBox->setEnabled(true);
}

void MainWindow::on_actionShowComment_triggered()
{
    toggleCommentButton();
}

void MainWindow::on_actionHideComment_triggered()
{
    toggleCommentButton();
}

void MainWindow::on_actionAbout_triggered()
{
    auto aboutApp = new DialogAppAbout();
    aboutApp->exec();
}

void MainWindow::on_actionAbout_Qt_triggered() const
{
    // about Qt
    QApplication::aboutQt();
}
void MainWindow::on_actionAbout_PcapPlusPlus_triggered()
{
    auto aboutPCapPlusPlus = new DialogPcapPlusPlusAbout();
    aboutPCapPlusPlus->exec();
}

void MainWindow::on_actionWebsite_triggered()
{
}

void MainWindow::on_actionOptions_triggered()
{
    // opens capture option dialog
    // captureOptionsDialog = new DialogCaptureOptions(availableDevs, defaultDev);
    captureOptionsDialog = new DialogCaptureOptions();

    /*initialise dialog value */
    captureOptionsDialog->availableDevs = availableDevs;
    captureOptionsDialog->defaultDev = defaultDev;
    captureOptionsDialog->autoCaptureFile = autoCaptureFile;
    captureOptionsDialog->afterKiloBytes = stopAfterKBytes;
    captureOptionsDialog->afterPacketCnt = stopAfterPackets;
    captureOptionsDialog->afterSeconds = stopAfterSeconds;
    captureOptionsDialog->captureFilter = captureFilter;
    captureOptionsDialog->autoCaptureFile = autoCaptureFile;

    // QObject::connect(captureOptionsDialog, &DialogCaptureOptions::setMWAvailableDev,
    //                  this, &MainWindow::setAvailableDevs);
    // QObject::connect(captureOptionsDialog, &DialogCaptureOptions::setMWDefaultDev,
    //                  this, &MainWindow::setDefaultDevInfo);

    captureOptionsDialog->exec();
    defaultDev = captureOptionsDialog->defaultDev;
    qDebug() << "Selected default device:" << defaultDev;
    autoCaptureFile = captureOptionsDialog->autoCaptureFile;
    stopAfterKBytes = captureOptionsDialog->afterKiloBytes;
    stopAfterPackets = captureOptionsDialog->afterPacketCnt;
    stopAfterSeconds = captureOptionsDialog->afterSeconds;
    captureFilter = captureOptionsDialog->captureFilter;
}

QString MainWindow::getSupportFileTypes(bool withComment) const
{
    QString supportedFileTypes = nullptr;

    for (auto type : SUPPORTED_FILE_TYPES_LIST)
    {
        if (supportedFileTypes == nullptr)
            supportedFileTypes += type[0];
        else
            supportedFileTypes += " ;; " + type[0];
    }
    return supportedFileTypes;
}

/* returns default file selection type for file dialog */
QString MainWindow::getDefaultFileType() const
{
    QStringList defaultFileTypeList = SUPPORTED_FILE_TYPES_LIST[defaultFileTypeIndex];
    QString defaultType = defaultFileTypeList[supportFileTypeIndex];
    return defaultType;
}

/* returns default file suffix */
QString MainWindow::getDefaultFileSuffix() const
{
    QStringList defaultFileTypeList = SUPPORTED_FILE_TYPES_LIST[defaultFileTypeIndex];
    QString defaultFileSuffix = defaultFileTypeList[fileSuffixIndex];
    return defaultFileSuffix;
}

/* returns file suffix for selected file type from file dialog operation */
QString MainWindow::getSelectedFileTypeSuffix(const QString &selectedFileType) const
{
    for (auto filetype : SUPPORTED_FILE_TYPES_LIST)
    {
        if (filetype[supportFileTypeIndex] == selectedFileType)
        {
            return filetype[fileSuffixIndex];
        }
    }
    return nullptr;
}

bool MainWindow::isSelectedFileTypeCompressed(const QString &selectedFileType) const
{
    for (auto filetype : SUPPORTED_FILE_TYPES_LIST)
    {
        if (filetype[supportFileTypeIndex] == selectedFileType)
        {
            if (filetype[fileCompressionIndex] == "true")
                return true;
            else
                return false;
        }
    }
    return false;
}

void MainWindow::captureScreenStatus(en::threadState state)
{
    switch (state)
    {
    case en::threadState::err_devOpen:
    case en::threadState::err_devNotFound:
    case en::threadState::err_filter:
        isLiveCapture = false;
        sb_CurrentSource->setText(tr("Failed to start live capture"));
        ui->statusbar->repaint();
        /* hide capture screen*/
        captureScreen->setHidden(true);
        ui->dummyScrollArea->setHidden(true);

        pcpp::multiPlatformSleep(3); // wait 5 secs to show err msg
        isFileSaved = true;
        setMenuButtons(appState::btn_init);

        break;

    case en::threadState::ok_stopLive:
        isLiveCapture = false;
        sb_CurrentSource->setText(tr("Live capture stopped"));
        setMenuButtons(appState::btn_start);
        break;

    case en::threadState::ok_startLive:
        isLiveCapture = true;
        sb_CurrentSource->setText(tr("Live capture in progress..."));
        break;

    case en::threadState::ok_stopFile:
        isFileLoading = false;
        isFileSaved = false;
        sb_CurrentSource->setText(tr("File load stopped "));
        /* hide capture screen*/
        captureScreen->setHidden(true);
        pcpp::multiPlatformSleep(3); // wait 5 secs to show err msg
        setMenuButtons(appState::btn_stop);

        // break;
    }
}