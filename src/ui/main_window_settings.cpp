#include "main_window.h"
#include "ui_main_window.h"

/*
method to set menu/toolbar buttons to correct enabled/disabled
state according to the current application state. Parameter
is current application state
*/
void MainWindow::setMenuButtons(appState btnChoice)
{

    switch (btnChoice)
    {
    case appState::btn_init:

        /* live capture */
        ui->actionStart->setEnabled(true);
        ui->actionStop->setEnabled(false);

        /* file menu */
        ui->actionSave->setEnabled(false);
        ui->actionSaveAs->setEnabled(false);

        ui->actionOpen->setEnabled(true);
        ui->actionClear->setEnabled(false);
        ui->actionOpenRecent->setEnabled(true);

        /* movement options */
        setPacketMovementCntl(false);
        ui->actionShowFilter->setVisible(true);
        ui->actionHideFilter->setVisible(false);
        ui->actionShowComment->setVisible(true);
        ui->actionHideComment->setVisible(false);
        ui->filterframe->setVisible(false);

        /* hide packet info on statusbar */
        hide_sbPacketInfo();

        /* display startup msg */
        sb_CurrentSource->setText(tr("Ready for load or live capture"));
        return;

    case appState::btn_start:
        // live capture
        ui->actionStart->setEnabled(false);
        ui->actionStop->setEnabled(true);
        // to add navigation buttons as false#

        // file menu
        ui->actionSave->setEnabled(false);
        ui->actionSaveAs->setEnabled(false);
        ui->actionOpenRecent->setEnabled(false);
        ui->actionOpen->setEnabled(false);
        ui->actionClear->setEnabled(false);

        setPacketMovementCntl(true);

        return;

    case appState::btn_file_saved:

        // live capture
        ui->actionStart->setEnabled(true);
        ui->actionStop->setEnabled(false);
        // to add navigation buttons as true

        // file menu
        ui->actionSave->setEnabled(false);
        ui->actionSaveAs->setEnabled(true);
        ui->actionOpenRecent->setEnabled(true);

        ui->actionOpen->setEnabled(true);
        ui->actionClear->setEnabled(true);

        setPacketMovementCntl(true);

        return;

    case appState::btn_openfile_loading:

        // live capture
        ui->actionStart->setEnabled(false);
        ui->actionStop->setEnabled(true);
        // to add navigation buttons as true

        // file menu
        ui->actionSave->setEnabled(false);
        ui->actionSaveAs->setEnabled(false);
        ui->actionOpenRecent->setEnabled(false);
        ui->actionOpen->setEnabled(false);
        ui->actionClear->setEnabled(false);

        setPacketMovementCntl(true);

        return;

    case appState::btn_stop:
    case appState::btn_openfile_afterload:

        // live capture
        ui->actionStart->setEnabled(true);
        ui->actionStop->setEnabled(false);
        // to add navigation buttons as true

        // file menu
        ui->actionSave->setEnabled(true);
        ui->actionSaveAs->setEnabled(true);
        ui->actionOpenRecent->setEnabled(true);
        ui->actionOpen->setEnabled(true);
        ui->actionClear->setEnabled(true);
        setPacketMovementCntl(true);
        return;

    case appState::btn_close:
    case appState::btn_close_cancel:
        return;

    case appState::btn_changed_file:

        return;

    case appState::btn_file_saving:
        // live capture
        ui->actionStart->setEnabled(false);
        ui->actionStop->setEnabled(false);
        // to add navigation buttons as false#

        // file menu
        ui->actionSave->setEnabled(false);
        ui->actionSaveAs->setEnabled(false);
        ui->actionOpenRecent->setEnabled(false);
        ui->actionOpen->setEnabled(false);
        ui->actionClear->setEnabled(false);
        setPacketMovementCntl(false);

        return;

    case appState::btn_quit:

        return;
    }
    // case btnStates::
}
void MainWindow::setPacketMovementCntl(bool isEnabled)
{
    ui->actionNext_Packet->setEnabled(isEnabled);
    ui->actionPrevious_Packet->setEnabled(isEnabled);
    ui->actionFirst_Packet->setEnabled(isEnabled);
    ui->actionLast_Packet->setEnabled(isEnabled);
    ui->actionShowFilter->setEnabled(isEnabled);
    ui->actionHideFilter->setEnabled(isEnabled);
    ui->actionShowComment->setEnabled(isEnabled);
    ui->actionHideComment->setEnabled(isEnabled);
}

void MainWindow::toggleFilterButton()
{
    if (ui->actionShowFilter->isVisible())
    {
        ui->actionShowFilter->setVisible(false);
        ui->actionHideFilter->setVisible(true);
        /* hide filter bar*/
        ui->filterBar->setVisible(false);
        ui->filterframe->setVisible(true);
    }
    else
    {
        ui->actionShowFilter->setVisible(true);
        ui->actionHideFilter->setVisible(false);
        /* show filter bar */
        ui->filterBar->setVisible(true);
        ui->filterframe->setVisible(false);
    }
}

void MainWindow::toggleCommentButton()
{
    if (ui->actionShowComment->isVisible())
    {
        ui->actionShowComment->setVisible(false);
        ui->actionHideComment->setVisible(true);
        captureScreen->setPacketDisplayFormat(arrowDisplay::packetAll);
    }
    else
    {
        ui->actionShowComment->setVisible(true);
        ui->actionHideComment->setVisible(false);
        captureScreen->setPacketDisplayFormat(arrowDisplay::packetDefault);
    }
}

/* reads application and user settings */
void MainWindow::readAppSettings()
{
    /* default mainwindow sizing */
    restoreGeometry(AppSetting::getGeometry());
    defaultSaveFileName = AppSetting::getDefaultSaveFileName();
    lastSavedFileFolder = AppSetting::getLastSaveFolder();
}

void MainWindow::writeAppSettings() const
{
    AppSetting::setGeometry(saveGeometry());
    AppSetting::setDefaultSaveFileName(defaultSaveFileName);
    AppSetting::setDefaultSaveFolder(defaultSaveFolder);
    AppSetting::setLastSaveFolder(lastSavedFileFolder);
}

void MainWindow::setRecentFilesMenu()
{
    if (AppSetting::isRecentFiles())
    {
        ui->actionOpenRecent->setVisible(true);
    }
    else
    {
        ui->actionOpenRecent->setVisible(false);
    }
}

void MainWindow::createStatusBar() const
{
    auto *splitter = new QSplitter();

    splitter->addWidget(sb_CurrentSource);
    splitter->addWidget(sb_PacketInfo);
    splitter->addWidget(sb_CurrentDevice);

    splitter->setStretchFactor(0, 3);
    splitter->setStretchFactor(1, 3);
    splitter->setStretchFactor(2, 1);

    statusBar()->addWidget(splitter, 1);
}

/* status bar info labels hide/show methods */
void MainWindow::hide_sbPacketInfo() { sb_PacketInfo->setVisible(false); }
void MainWindow::show_sbPacketInfo() { sb_PacketInfo->setVisible(true); }

void MainWindow::hide_sbCurrentDevice() { sb_CurrentDevice->setVisible(false); }
void MainWindow::show_sbCurrentDevice() { sb_CurrentDevice->setVisible(true); }

void MainWindow::hide_sbCurrentSource() { sb_CurrentSource->setVisible(false); }
void MainWindow::show_sbCurrentSource() { sb_CurrentSource->setVisible(true); }

void MainWindow::set_sbPacketInfo(int packetsReceived, int packetsDisplayed, int packetsDropped)
{
    QString packetinfo = " ";
    /* check zero rec'd, otherwise display blank */
    if (packetsReceived > 0)
    {
        packetinfo.append((QString(tr("Packets received: %1 - Displayed: %2 (%3%)"))
                               .arg(packetsReceived))
                              .arg(packetsDisplayed)
                              .arg((100.0 * packetsDisplayed) / packetsReceived, 0, 'f', 1));

        if (packetsDropped > 0)
        {
            packetinfo.append((QString(tr(" - Dropped: %1     ").arg(packetsDropped))));
        }
        else
        {
            /* add spacer from device box*/
            packetinfo.append("    ");
        }
    }
    sb_PacketInfo->setText(packetinfo);
    //}
}
void MainWindow::set_sbCurrentSource(QString message, bool isFile)
{
    QString currentsource;

    if (isFile)
    {
        currentsource.append((QString(tr("File: %1.%2 "))
                                  .arg(QFileInfo(message).baseName()))
                                 .arg(QFileInfo(message).completeSuffix()));
    }
    else
    {
        currentsource = message;
    }
    sb_CurrentSource->setText(currentsource);
}

void MainWindow::set_sbCurrentDevice(QStringList currentDevice)
{

    sb_CurrentDevice->setText(QString(tr("Device: %1")).arg(currentDevice.at(0)));
}

/*
    reads recent filters from settings.
    removes any invalid filters / updating settings
*/
QStringList MainWindow::readRecentFilters()
{
    QStringList filterList = AppSetting::getRecentFilterList();
    QStringList cleanFilterList;
    for (int i = 0; i < filterList.size(); ++i)
    {
        /* if filter valid syntax accept it */
        if (filterBPF->setFilter(filterList.at(i)))
        {
            // qDebug() << "Reading filter: " << filterList.at(i);
            cleanFilterList.append(filterList.at(i));
        }
    }

    return cleanFilterList;
}
