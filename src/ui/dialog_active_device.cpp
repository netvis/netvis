#include "dialog_active_device.h"
#include "ui_dialog_active_device.h"

DialogActiveDevice::DialogActiveDevice(QWidget *parent) : QDialog(parent),
                                                          ui(new Ui::DialogActiveDevice)
{
    ui->setupUi(this);
    this->setObjectName("File capture device");
}

DialogActiveDevice::~DialogActiveDevice()
{
    delete ui;
}

void DialogActiveDevice::init(QStringList activeDev)
{
    devName = activeDev.at(0);
    ui->ipv4Input->setText(activeDev.at(1));
    ui->gatewayInput->setText(activeDev.at(2));
}

void DialogActiveDevice::on_ok_clicked()
{
    QStringList devList;
    devList.append(devName);
    devList.append(ui->ipv4Input->toPlainText());
    devList.append(ui->gatewayInput->toPlainText());
    devList.append("*Link layer*");
    devList.append(ui->ipv6input->toPlainText());

    emit updateDevList(devList);
    this->close();
}

void DialogActiveDevice::on_cancel_clicked()
{
    this->close();
}
