#include "dialog_pcapplusplus_about.h"
#include "ui_dialog_pcapplusplus_about.h"

DialogPcapPlusPlusAbout::DialogPcapPlusPlusAbout(QWidget *parent)
    : QDialog(parent),
      ui(new Ui::DialogPcapPlusPlusAbout)
{

    ui->setupUi(this);
    setWindowTitle(QApplication::applicationName());

    ui->appName->setText("PcapPlusPlus");
    ui->appVersion->setText("Version: " + QString::fromStdString(pcpp::getPcapPlusPlusVersionFull()));

    QString appbuild = QString::fromStdString(pcpp::getGitInfo()) +
                       " (" + QString::fromStdString(pcpp::getGitBranch()) + ") " +
                       QString::fromStdString(pcpp::getBuildDateTime());
    ui->appBuild->setText("Build: " + appbuild);
    ui->appDescription->setText("A multi-platform C++ library for capturing, parsing and crafting of network packets. It is designed to be efficient, powerful and easy to use.");

    ui->OrgName->setText("PcapPlusPlus");

    ui->OrgDomain->setTextInteractionFlags(Qt::LinksAccessibleByMouse);
    ui->OrgDomain->setOpenExternalLinks(true);
    ui->OrgDomain->setTextFormat(Qt::RichText);
    QString home = "Home: <a href=\\https://pcapplusplus.github.io\\>https:/pcapplusplus.github.io</a>";
    ui->OrgDomain->setText(home);

    ui->github->setTextInteractionFlags(Qt::LinksAccessibleByMouse);
    ui->github->setOpenExternalLinks(true);
    ui->github->setTextFormat(Qt::RichText);
    QString github = "Source: <a href=\\https://github.com/seladb/PcapPlusPlus\\>https:/github.com/seladb/PcapPlusPlus</a>";
    ui->github->setText(github);
}

DialogPcapPlusPlusAbout::~DialogPcapPlusPlusAbout()
{
    delete ui;
}

void DialogPcapPlusPlusAbout::on_buttonBox_accepted()
{
    /* closes dialog box */
    QDialog::close();
}