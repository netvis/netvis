#include "dialog_app_about.h"
#include "ui_dialog_app_about.h"
#include "app_version.h"
#include "git_build.h"

DialogAppAbout::DialogAppAbout(QWidget *parent)
    : QDialog(parent),
      ui(new Ui::DialogAppAbout)
{

    ui->setupUi(this);
    setWindowTitle(QApplication::applicationName());

    ui->appName->setText(QApplication::applicationName());
    ui->appVersion->setText("Version: " + QApplication::applicationVersion());

    ui->appBuild->setText("Build: " + QString::fromStdString(GIT_REV) + " (" + GIT_BRANCH + ") " + APP_BUILD_DATE);
    ui->appDescription->setText(APP_DESCRIPTION);
    ui->OrgDomain->setText(ORG_DOMAIN);
    ui->OrgName->setText(ORG_NAME);
    if (ORG_DOMAIN != "")
    {
        ui->OrgDomain->setTextInteractionFlags(Qt::LinksAccessibleByMouse);
        ui->OrgDomain->setOpenExternalLinks(true);
        ui->OrgDomain->setTextFormat(Qt::RichText);
        ui->OrgDomain->setText(ORG_DOMAIN_LINK);
    }

    if (GIT_HUB_LINK != "")
    {
        ui->github->setTextInteractionFlags(Qt::LinksAccessibleByMouse);
        ui->github->setOpenExternalLinks(true);
        ui->github->setTextFormat(Qt::RichText);
        ui->github->setText(GIT_HUB_LINK);
    }
}

DialogAppAbout::~DialogAppAbout()
{
    delete ui;
}

void DialogAppAbout::on_buttonBox_accepted()
{
    /* closes dialog box */
    QDialog::close();
}