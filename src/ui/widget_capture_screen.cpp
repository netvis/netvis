#include "widget_capture_screen.h"
#include "ui_widget_capture_screen.h"

WidgetCaptureScreen::WidgetCaptureScreen(QStringList defaultDev,
                                         QStatusBar *statusBar,
                                         QWidget *parent) : QWidget(parent),
                                                            ui(new Ui::WidgetCaptureScreen)
{
    ui->setupUi(this);
    this->setStyleSheet("background-color: #ffffff");
    this->statusBar = statusBar;
    this->defaultDev = defaultDev;

    this->defaultDevName = defaultDev.at(0).toStdString();

    filterBPF = new CaptureFilterBPF();

    init();
}

void WidgetCaptureScreen::init()
{
    /* create arrow diagram layout */
    inner = new QFrame(ui->scrollArea);
    packetLayout = new QVBoxLayout();
    packetLayout->setSpacing(0);
    packetLayout->setMargin(0);
    inner->setLayout(packetLayout);
    ui->scrollArea->setWidget(inner);

    /* global parameters */
    capturePacketThread = nullptr;
    isLiveCapture = false;
    isDisplayFilter = false;
    displayfilter = nullptr;
    isFileComment = false;
    isFileLoading = false;

    /* capture session settings */
    stopAfterSeconds = 0;
    stopAfterKBytes = 0;
    stopAfterPackets = 0;
    captureFilter = nullptr;
    isShowAnalysis = false;
    isShowComment = false;
}

WidgetCaptureScreen::~WidgetCaptureScreen()
{
    if (isLiveCapture)
    {
        capturePacketThread->stopLiveCapture();
    }

    rawPacketList.clear();
    packetCommentList.clear();
    // packetAnalysisList.clear();

    delete ui;
}

void WidgetCaptureScreen::threadStatus(en::threadState state)
{
    switch (state)
    {
    case en::threadState::err_devOpen:
    case en::threadState::err_devNotFound:
        /* hide the widget  */
        qDebug() << "wcs - failed to start live capture signal";
        isLiveCapture = false;
        break;

    case en::threadState::err_startLive:
        qDebug() << "wcs - error during live capture signal";
        isLiveCapture = false;
        break;

    case en::threadState::ok_startLive:
        isLiveCapture = true;
        // qDebug() << "ok start live";
        break;
    case en::threadState::err_filter:
        qCritical() << "unable to apply capture filter, filter invalid";
        break;
    case en::threadState::ok_stopFile:
        isFileLoading = false;
        // qDebug() << "ok stop file";
        break;

    case en::threadState::ok_stopLive:
        qDebug() << "wcs - stopping live capture signal";
        isLiveCapture = false;
    }

    emit emitThreadStatus(state);
}

void WidgetCaptureScreen::prepareOpenCaptureFile(const QString &fileName, const QString &capturefilter)
{
    /* initialise session countyers */
    packetsCaptured = 0;
    secondsCaptured = 0;
    bytesCaptured = 0;

    packetNumberFocusIndex = -1;
    if (capturePacketThread == nullptr)
    {
        capturePacketThread = new CapturePacketThread(false,
                                                      &rawPacketList,
                                                      &packetCommentList,
                                                      statusBar,
                                                      &defaultDevName);
        connect(capturePacketThread, &CapturePacketThread::emitNewCapturePacket,
                this, &WidgetCaptureScreen::renderDiagram);
        connect(capturePacketThread, &CapturePacketThread::emitCaptureSessionSignal,
                this, &WidgetCaptureScreen::threadStatus);
    }

    isFileLoading = true;

    // initialise displayed packet counter
    displayedPacketCnt = 0;
    qDebug() << "Preparing to open file: " << fileName;
    /* open capture file and start displaying packet info */
    if (capturePacketThread->openCaptureFile(fileName, capturefilter))
    {
        qInfo() << "File successfully read ";
    }
    else
    {
        qCritical() << "Error reading file ";
    }

    isFileLoading = false;

    return;
}

void WidgetCaptureScreen::prepareOpenCaptureLive(const QString &capturefilter)
{
    /* initialise session countyers */
    packetsCaptured = 0;
    secondsCaptured = 0;
    bytesCaptured = 0;

    packetNumberFocusIndex = -1;
    auto dev = pcpp::PcapLiveDeviceList::getInstance().getPcapLiveDeviceByName(defaultDev.at(0).toStdString());

    isLiveCapture = true;
    if (capturePacketThread == nullptr)
    {
        capturePacketThread = new CapturePacketThread(true,
                                                      &rawPacketList,
                                                      &packetCommentList,
                                                      statusBar,
                                                      &defaultDevName);
        connect(capturePacketThread, &CapturePacketThread::emitNewCapturePacket,
                this, &WidgetCaptureScreen::renderDiagram);
        connect(capturePacketThread, &CapturePacketThread::emitCaptureSessionSignal,
                this, &WidgetCaptureScreen::threadStatus);
    }

    /* initialise displayed packet counter */
    displayedPacketCnt = 0;

    /* start QThread */
    capturePacketThread->openCaptureLive(capturefilter);
    return;
}

// NEED TO EXPAND TO CLOSE ANALYSIS THREADS
void WidgetCaptureScreen::stopLiveCapture()
{
    capturePacketThread->stopLiveCapture();
    isLiveCapture = false;
}

// NEED TO EXPAND TO CLOSE ANALYSIS THREADS?
void WidgetCaptureScreen::stopFileLoading()
{
    capturePacketThread->stopFileLoading();
    isFileLoading = false;
}

// void WidgetCaptureScreen::liveDebug(size_t rawPacketListSize)
void WidgetCaptureScreen::renderDiagram(int packetNumber)
{

    // qDebug() << QString("Packet %1 received ").arg(packetNumber);
    /* update value of last packet recieved */
    lastPacketNumberIndex = packetNumber - 1;
    /* parse the packet */
    pcpp::Packet parsedPacket(rawPacketList.at(lastPacketNumberIndex));

    /* initialise packet display flag */
    bool isDisplayPacket = true;

    /*if display filter, test this packet for match */
    if (isDisplayFilter &&
        (!filterBPF->matchPacketvsFilter(rawPacketList.at(lastPacketNumberIndex))))
        isDisplayPacket = false;

    /* append to displayedPacketsList/hiddenPacketsList */
    if (isDisplayPacket)
        display_hidePacketsList.append(true);
    else
        display_hidePacketsList.append(false);

    rendorArrowWidget(&parsedPacket, packetNumber, isDisplayPacket);
    // qDebug() << QString("Arrow %1 rendered").arg(packetNumber);

    /*
    increment displayed packet count and packet information
    if this is first display packet. set focus to first arrow
    */
    if (isDisplayPacket)
    {
        // increment display count
        displayedPacketCnt += 1;

        /* packetNumberFocusIndex=-1 means no previous packet selected */
        if (displayedPacketCnt == 1 && packetNumberFocusIndex == -1)
        {
            rendorPacketInfoWidget(&parsedPacket);
            rendorPacketHexWidget(&parsedPacket);
            // qDebug() << QString("Arrow %1 info rendered").arg(packetNumber);
            setSelectArrow(lastPacketNumberIndex, true);
            packetNumberFocusIndex = lastPacketNumberIndex;
        }
    }

    /* */

    /* update status bar packet received/displayed counters */
    emit packetInfo(packetNumber, displayedPacketCnt, droppedPacketCnt);
}

/*
    refreshes arrow diagram with all arrows matching the filter, which
    can be either a BPF string or nullptr (=show all packet arrows).
*/
void WidgetCaptureScreen::setDisplayFilter(const QString &filterString)
{
    /* test for valid filter before setting it for display of packets */
    if (filterBPF->setFilter(filterString))
    {

        /* display current packet arrows meeting filter criteria */
        qDebug() << "refreshing packet arrows on diagram using filter: " << filterString;

        /* save the filter settings */
        displayfilter = filterString;
        bool isDisplayPacket;
        /* reset displayed packet counter */
        displayedPacketCnt = 0;
        /* reset position of arrow currently in focus. Set -1
        in case no packets are displayable */
        packetNumberFocusIndex = -1;

        /* loop over arrow widgets testing rawpacket for match
        and display */
        // qDebug() << "packet layout count=" << packetLayout->count();
        for (int i = 0; i < packetLayout->count(); i++)
        {
            /* initialise packet display flag false*/
            isDisplayPacket = false;

            /* if packet match set isDisplayPacket flag true*/
            if (filterBPF->matchPacketvsFilter(rawPacketList.at(i)))
            {
                isDisplayPacket = true;

                QWidget *item = packetLayout->itemAt(i)->widget();
                if (item != nullptr)
                {
                    item->setVisible(isDisplayPacket);
                }
                /* increment displayed arrow count */
                displayedPacketCnt += 1;
                /*
                if packFocusIndex = -1 then set it to first
                displayable packet's index
                */
                if (packetNumberFocusIndex == -1)
                {
                    packetNumberFocusIndex = i;
                }
            }
            /* reset display/hide list */
            display_hidePacketsList[i] = isDisplayPacket;
            displayArrow(i, isDisplayPacket);
        }
        /* move focus to first displayable packet */
        packetNumberFocusIndex = -1;
        onArrowMove(arrowMove::first);

        /* update status bar packet received/displayed counters */
        emit packetInfo(lastPacketNumberIndex + 1, displayedPacketCnt, droppedPacketCnt);
    }
}

void WidgetCaptureScreen::clearDisplayFilter()
{
    /* clear current filters */
    isDisplayFilter = false;
    displayfilter = nullptr;

    /* reset displayed packets count to nr of packets received */
    displayedPacketCnt = lastPacketNumberIndex;

    /* loop over hiddened arrow widgets setting visible */
    for (int i = 0; i <= lastPacketNumberIndex; i++)
    {
        if (!display_hidePacketsList.at(i))
        {
            display_hidePacketsList[i] = true;
            displayArrow(i, true);
        }
    }

    /* move focus to first packet */
    packetNumberFocusIndex = 0;
    setSelectArrow(packetNumberFocusIndex, true);

    /* update status bar packet received/displayed counters */
    emit packetInfo(displayedPacketCnt, displayedPacketCnt, droppedPacketCnt);
}

/* clear the currently display packet arrow diagram */
void WidgetCaptureScreen::clearDiagram()
{
    qDebug() << "clearing current packet arrows from diagram";
    /* display waiting cursor */
    QApplication::setOverrideCursor(Qt::WaitCursor);

    /* hide scrollarea whilst deleting arrows */
    ui->scrollArea->setVisible(false);
    ui->scrollArea->repaint();

    /* clear display widgets */
    ui->treeWidget->clear();
    qDebug() << "tree widget cleared";
    ui->textBrowser->clear();
    qDebug() << "text browser cleared";

    QLayoutItem *child;
    while ((child = packetLayout->takeAt(0)) != nullptr)
    {
        delete child->widget(); // delete the widget
        delete child;           // delete the layout item
    }
    qDebug() << "arrow widgets deleted cleared";
    ui->scrollArea->setVisible(true);
    /* restore default cursor */
    QApplication::restoreOverrideCursor();
}

/* close current diagram clearing screen and emptying packet containers */
void WidgetCaptureScreen::closeDiagram()
{
    clearDiagram();
    qDebug() << "clearing containers";
    rawPacketList.clear();
    qDebug() << "rawpacketlist cleared";
    // packetAnalysisList.clear();
    packetCommentList.clear();
    qDebug() << "packetcomments cleared";
}

bool WidgetCaptureScreen::prepareSaveCaptureFile(const QString &savefileName,
                                                 bool compression) const
{
    /* get file extension format to call correct function */
    cmm::fileFormat filetype = cmm::getFileFormat(QFileInfo(savefileName).suffix());

    switch (filetype)
    {
    case cmm::fileFormat::pcap:
        return capturePacketThread->saveToPcapFile(savefileName);

    case cmm::fileFormat::pcapng:
        return capturePacketThread->saveToPcapNgFile(savefileName,
                                                     compression);
    default:
        // unrecognised file type
        qCritical() << "Unrecognised file type: '" << savefileName << "'";
    }

    return false;
}
