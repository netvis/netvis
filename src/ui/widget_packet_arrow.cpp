#include "widget_packet_arrow.h"
#include "ui_widget_packet_arrow.h"

WidgetPacketArrow::WidgetPacketArrow(QWidget *parent) : QWidget(parent), ui(new Ui::WidgetPacketArrow)
{
    ui->setupUi(this);
    ui->packetNumber->setAttribute(Qt::WA_TranslucentBackground, true);
    ui->protocol->setAttribute(Qt::WA_TranslucentBackground, true);
    ui->protocolList->setAttribute(Qt::WA_TranslucentBackground, true);
    ui->srcIPAddr->setAttribute(Qt::WA_TranslucentBackground, true);
    ui->dstIPAddr->setAttribute(Qt::WA_TranslucentBackground, true);
    ui->arrowDiagram->setAttribute(Qt::WA_TranslucentBackground, true);
    ui->analysisComment->setAttribute(Qt::WA_TranslucentBackground, true);
    ui->userComment->setAttribute(Qt::WA_TranslucentBackground, true);

    toggleAnalysisComments(false);
    toggleUserComments(false);

    // clientDevDetails.append("eth0");
    // clientDevDetails.append("192.168.1.109");
    // clientDevDetails.append("192.168.1.254");
}

WidgetPacketArrow::~WidgetPacketArrow()
{
    delete ui;
}

void WidgetPacketArrow::setClientDevDetails(QStringList defaultDev)
{
    this->clientDevIPv4 = defaultDev.at(1).toStdString();
    if (defaultDev.size() == 5)
        this->clientDevIPv6 = defaultDev.at(4).toStdString();
}

void WidgetPacketArrow::setPacketNumber(int number)
{
    this->packetNumber = std::to_string(number);
}

void WidgetPacketArrow::setPacketLength(int length)
{
    this->packetLength = std::to_string(length);
}

void WidgetPacketArrow::setPackerTimeStamp(std::string time)
{
    this->packetTimeStamp = time;
}

void WidgetPacketArrow::setSrcAddress(std::string address)
{
    this->packetSrcAddress = address;
}

void WidgetPacketArrow::setDstAddress(std::string address)
{
    this->packetDstAddress = address;
}

void WidgetPacketArrow::setLinkProtocol(std::string protocol)
{
    this->packetLinkProtocol = protocol;
}

void WidgetPacketArrow::setInternetProtocol(std::string protocol)
{
    this->packetInternetProtocol = protocol;
}

void WidgetPacketArrow::setTransportProtocol(std::string protocol)
{
    this->packetTransportProtocol = protocol;
}

void WidgetPacketArrow::setSessionProtocol(std::string protocol)
{
    this->packetSessionProtocol = protocol;
}

void WidgetPacketArrow::setPresentationProtocol(std::string protocol)
{
    this->packetPresentationProtocol = protocol;
}

void WidgetPacketArrow::setApplicationProtocol(std::string protocol)
{
    this->packetPresentationProtocol = protocol;
}

void WidgetPacketArrow::setForegroundColour(std::string colour)
{
    this->foregroundColour = colour;
}

void WidgetPacketArrow::setBackgroundColour(std::string colour)
{
    this->backgroundColour = colour;
}

void WidgetPacketArrow::selectPacket(bool selected)
{
    this->packetSelected = selected;
}

void WidgetPacketArrow::setAnalysisComments(std::string comments)
{
    this->analysisComments = comments;
}

void WidgetPacketArrow::setUserComments(std::string comments)
{
    this->userCommments = comments;
}

void WidgetPacketArrow::toggleAnalysisComments(bool toggle)
{
    ui->analysisComment->setVisible(toggle);
}

void WidgetPacketArrow::toggleUserComments(bool toggle)
{
    ui->userComment->setVisible(toggle);
}

void WidgetPacketArrow::mousePressEvent(QMouseEvent *event)
{
    emit arrowClicked(std::stoi(packetNumber) - 1);
    this->packetSelected = true;
}

void WidgetPacketArrow::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    auto pal = QPalette();

    scene = new QGraphicsScene(this);

    setPacketArrow();
    // setHostLines(painter);
    setPacketInformation();
    setPacketColour(pal);

    ui->arrowDiagram->setScene(scene);
}

void WidgetPacketArrow::setPacketArrow()
{
    /* make packet arrow */
    QPolygon packetArrow;

    // if source = default ip, then: src ----> dst
    if (packetSrcAddress == clientDevIPv4) // || packetSrcAddress == clientDevIPv6)
    {
        FLIP = false;
        // this missed DNS requets and reponses -> must also test
        packetArrow << QPoint(153, 50);
        packetArrow << QPoint(838, 50);
        packetArrow << QPoint(838, 60);
        packetArrow << QPoint(848, 50);
        packetArrow << QPoint(838, 40);
        packetArrow << QPoint(838, 50);
    }
    // else dst = default ip, then: dst <---- src
    else
    {
        FLIP = true;
        packetArrow << QPoint(847, 50);
        packetArrow << QPoint(162, 50);
        packetArrow << QPoint(162, 60);
        packetArrow << QPoint(152, 50);
        packetArrow << QPoint(162, 40);
        packetArrow << QPoint(162, 50);
    }

    /* create arrow pen */
    QPen arrowPen;
    arrowPen.setWidth(2);
    if (packetSelected)
    {
        arrowPen.setWidth(4);
        ui->packetNumber->setTextFormat(Qt::RichText);
        ui->srcIPAddr->setTextFormat(Qt::RichText);
        ui->dstIPAddr->setTextFormat(Qt::RichText);
        ui->protocol->setTextFormat(Qt::RichText);
        ui->protocolList->setTextFormat(Qt::RichText);
    }
    arrowPen.setColor(QColor(QString::fromStdString(foregroundColour)));
    arrowPen.setJoinStyle(Qt::MiterJoin);
    // painter.setPen(arrowPen);

    /* create arrow brush */
    QBrush arrowBrush;
    arrowBrush.setColor(Qt::red);

    /* draw packet arrow */
    QGraphicsPolygonItem *arrow = scene->addPolygon(packetArrow, arrowPen, arrowBrush);
    arrow->setFlag(QGraphicsItem::ItemIsSelectable);
}

void WidgetPacketArrow::setPacketInformation()
{
    /* general packet information */
    ui->analysisComment->setText(QString::fromStdString(analysisComments));
    ui->userComment->setText(QString::fromStdString((userCommments)));

    std::vector<std::string> tempProtList = {packetLinkProtocol,
                                             packetInternetProtocol,
                                             packetTransportProtocol,
                                             packetSessionProtocol,
                                             packetPresentationProtocol,
                                             packetApplicationProtocol};
    QString protocolList;
    for (int i = 0; i < tempProtList.size(); i++)
    {
        if (tempProtList.at(i) == "")
        {
            continue;
        }
        if (i == tempProtList.size() - 1)
        {
            protocolList.append(QString::fromStdString(tempProtList.at(i)));
            break;
        }
        protocolList.append(QString::fromStdString(tempProtList.at(i)) + ": ");
    }

    if (packetSelected)
    {
        ui->packetNumber->setText(QString::fromStdString("<b>" + packetNumber +
                                                         " " + packetTimeStamp + "</b>"));
        ui->protocolList->setText("<b>" + protocolList + "<b/>");
    }
    else
    {
        ui->packetNumber->setText(QString::fromStdString(packetNumber + " " + packetTimeStamp));
        ui->protocolList->setText(protocolList);
    }

    for (int i = tempProtList.size() - 1; i >= 0; i--)
    {
        if (tempProtList.at(i) != "" && packetSelected == false)
        {
            ui->protocol->setText(QString::fromStdString(tempProtList.at(i)));
            break;
        }

        if (tempProtList.at(i) != "" && packetSelected == true)
        {
            ui->protocol->setText(QString::fromStdString("<b>" + tempProtList.at(i)) + "</b>");
            break;
        }
    }

    /* client and host information */
    if (FLIP == true && packetSelected == true)
    {
        ui->srcIPAddr->setText(QString::fromStdString("<b>" + packetSrcAddress + "</b>"));
        ui->dstIPAddr->setText(QString::fromStdString("<b>" + packetDstAddress + "</b>"));
    }
    if (FLIP == true && packetSelected == false)
    {
        ui->srcIPAddr->setText(QString::fromStdString(packetSrcAddress));
        ui->dstIPAddr->setText(QString::fromStdString(packetDstAddress));
    }
    if (FLIP == false && packetSelected == true)
    {
        ui->srcIPAddr->setText(QString::fromStdString("<b>" + packetDstAddress + "</b>"));
        ui->dstIPAddr->setText(QString::fromStdString("<b>" + packetSrcAddress + "</b>"));
    }
    if (FLIP == false && packetSelected == false)
    {
        ui->srcIPAddr->setText(QString::fromStdString(packetDstAddress));
        ui->dstIPAddr->setText(QString::fromStdString(packetSrcAddress));
    }
}

void WidgetPacketArrow::setPacketColour(QPalette &pal)
{
    pal.setColor(QPalette::Window, QString::fromStdString(backgroundColour));
    this->setAutoFillBackground(true);
    this->setPalette(pal);
}