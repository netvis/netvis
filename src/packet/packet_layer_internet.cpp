#include "packet_layer_internet.h"

namespace pkl
{
    void setIPv4Data(const pcpp::Packet &packet, QTreeWidget *packetInfo)
    {
        if (packet.isPacketOfType(pcpp::IPv4))
        {
            pcpp::IPv4Layer const *ip4Layer = packet.getLayerOfType<pcpp::IPv4Layer>();
            if (ip4Layer == nullptr)
            {
                qDebug() << "Something went wrong, couldn't find IPv4 layer";
            }
            else
            {
                auto *layerRoot = new QTreeWidgetItem(packetInfo);

                addRoot(packetInfo, layerRoot, QString::fromStdString("Internet Protocol Version 4"));
                addChild(layerRoot, QString("Version: "), QString::number(ip4Layer->getIPv4Header()->ipVersion));
                addChild(layerRoot, QString("Header Length: "), QString::number(ip4Layer->getHeaderLen()));
                addChild(layerRoot, QString("Differentiated Services Field: "), QString::number(ip4Layer->getIPv4Header()->typeOfService));
                addChild(layerRoot, QString("Total Length: "), QString::number(ip4Layer->getIPv4Header()->totalLength));
                addChild(layerRoot, QString("Identification: "), QString::number(ip4Layer->getIPv4Header()->ipId));
                addChild(layerRoot, QString("Flags: "), QString::number(ip4Layer->getFragmentFlags()));
                addChild(layerRoot, QString("Fragment Offset: "), QString::number(ip4Layer->getFragmentOffset()));
                addChild(layerRoot, QString("Time to Live: "), QString::number(ip4Layer->getIPv4Header()->timeToLive));
                addChild(layerRoot, QString("Protocol: "), QString::number(ip4Layer->getIPv4Header()->protocol));
                addChild(layerRoot, QString("Header Checksum: "), QString::number(ip4Layer->getIPv4Header()->headerChecksum));
                addChild(layerRoot, QString("Source Address: "), QString::fromStdString(ip4Layer->getSrcIPAddress().toString()));
                addChild(layerRoot, QString("Destination Address: "), QString::fromStdString(ip4Layer->getDstIPAddress().toString()));
            }
        }
    }

    void setIPv6Data(const pcpp::Packet &packet, QTreeWidget *packetInfo)
    {
        if (packet.isPacketOfType(pcpp::IPv6))
        {
            pcpp::IPv6Layer const *ip6Layer = packet.getLayerOfType<pcpp::IPv6Layer>();
            if (ip6Layer == nullptr)
            {
                qDebug() << "Something went wrong, couldn't find IPv6 layer";
            }
            else
            {
                auto *layerRoot = new QTreeWidgetItem(packetInfo);

                addRoot(packetInfo, layerRoot, QString::fromStdString("Internet Protocol Version 6"));
                addChild(layerRoot, QString("Version: "), QString::number(ip6Layer->getIPv6Header()->ipVersion));
                addChild(layerRoot, QString("Traffic Class: "), QString::number(ip6Layer->getIPv6Header()->trafficClass));
                // addChild(layerRoot, QString("Flow Label: "), QString::number(ip6Layer->getIPv6Header()->flowLabel));
                addChild(layerRoot, QString("Payload Header: "), QString::number(ip6Layer->getIPv6Header()->payloadLength));
                addChild(layerRoot, QString("Next Header: "), QString::number(ip6Layer->getIPv6Header()->nextHeader));
                addChild(layerRoot, QString("Hop Limit: "), QString::number(ip6Layer->getIPv6Header()->nextHeader));
                addChild(layerRoot, QString("Source Address: "), QString::fromStdString(ip6Layer->getSrcIPv6Address().toString()));
                addChild(layerRoot, QString("Destination Address: "), QString::fromStdString(ip6Layer->getDstIPv6Address().toString()));
            }
        }
    }

    void setICMPData(const pcpp::Packet &packet, QTreeWidget *packetInfo)
    {
        if (packet.isPacketOfType(pcpp::ICMP))
        {
            pcpp::IcmpLayer *icmpLayer = packet.getLayerOfType<pcpp::IcmpLayer>();
            if (icmpLayer == nullptr)
            {
                qDebug() << "Something went wrong, couldn't find ICMP layer";
            }
            else
            {
                auto *layerRoot = new QTreeWidgetItem(packetInfo);

                addRoot(packetInfo, layerRoot, QString::fromStdString("Internet Control Message Protocol"));
                addChild(layerRoot, QString("Type: "), QString::number(icmpLayer->getMessageType()) + QString::fromStdString(" " + getICMPType(icmpLayer->getMessageType())));
                addChild(layerRoot, QString("Code: "), QString::number(icmpLayer->getIcmpHeader()->code));
                addChild(layerRoot, QString("Checksum: "), QString::number(icmpLayer->getIcmpHeader()->checksum));

                if (icmpLayer->isMessageOfType(pcpp::ICMP_ECHO_REQUEST))
                {
                    addChild(layerRoot, QString("Identifier: "), QString::number(icmpLayer->getEchoRequestData()->header->id));
                    addChild(layerRoot, QString("Sequence Number: "), QString::number(icmpLayer->getEchoRequestData()->header->sequence));
                    addChild(layerRoot, QString("Time Stamp: "), QString::number(icmpLayer->getEchoRequestData()->header->timestamp));

                    auto *dataItem = new QTreeWidgetItem(layerRoot);
                    addSubRoot(layerRoot, dataItem, QString("Data (" + QString::number(icmpLayer->getEchoRequestData()->dataLength) + " bytes)"));
                    // addChild(layerRoot, QString("Data : "), QString::number(icmpLayer->getEchoRequestData()->data));
                    addChild(layerRoot, QString("Length : "), QString::number(icmpLayer->getEchoRequestData()->dataLength));
                }

                if (icmpLayer->isMessageOfType(pcpp::ICMP_ECHO_REPLY))
                {
                    addChild(layerRoot, QString("Identifier: "), QString::number(icmpLayer->getEchoReplyData()->header->id));
                    addChild(layerRoot, QString("Sequence Number: "), QString::number(icmpLayer->getEchoReplyData()->header->sequence));
                    addChild(layerRoot, QString("Time Stamp: "), QString::number(icmpLayer->getEchoReplyData()->header->timestamp));

                    auto *dataItem = new QTreeWidgetItem(layerRoot);
                    addSubRoot(layerRoot, dataItem, QString("Data (" + QString::number(icmpLayer->getEchoReplyData()->dataLength) + " bytes)"));
                    // addChild(layerRoot, QString("Data : "), QString::number(icmpLayer->getEchoReplyData()->data));
                    addChild(layerRoot, QString("Length : "), QString::number(icmpLayer->getEchoReplyData()->dataLength));
                }

                if (icmpLayer->isMessageOfType(pcpp::ICMP_TIMESTAMP_REQUEST))
                {
                    addChild(layerRoot, QString("Identifier: "), QString::number(icmpLayer->getTimestampRequestData()->id));
                    addChild(layerRoot, QString("Sequence Number: "), QString::number(icmpLayer->getTimestampRequestData()->sequence));
                    addChild(layerRoot, QString("Originate Time Stamp: "), QString::number(icmpLayer->getTimestampRequestData()->originateTimestamp));
                    addChild(layerRoot, QString("Recieve Time Stamp: "), QString::number(icmpLayer->getTimestampRequestData()->receiveTimestamp));
                    addChild(layerRoot, QString("Transmit Time Stamp: "), QString::number(icmpLayer->getTimestampRequestData()->transmitTimestamp));
                }

                if (icmpLayer->isMessageOfType(pcpp::ICMP_TIMESTAMP_REPLY))
                {
                    addChild(layerRoot, QString("Identifier: "), QString::number(icmpLayer->getTimestampReplyData()->id));
                    addChild(layerRoot, QString("Sequence Number: "), QString::number(icmpLayer->getTimestampReplyData()->sequence));
                    addChild(layerRoot, QString("Originate Time Stamp: "), QString::number(icmpLayer->getTimestampReplyData()->originateTimestamp));
                    addChild(layerRoot, QString("Recieve Time Stamp: "), QString::number(icmpLayer->getTimestampReplyData()->receiveTimestamp));
                    addChild(layerRoot, QString("Transmit Time Stamp: "), QString::number(icmpLayer->getTimestampReplyData()->transmitTimestamp));
                }

                if (icmpLayer->isMessageOfType(pcpp::ICMP_DEST_UNREACHABLE))
                {
                    addChild(layerRoot, QString("Unused: "), QString::number(icmpLayer->getDestUnreachableData()->unused));
                    addChild(layerRoot, QString("MTU: "), QString::number(icmpLayer->getDestUnreachableData()->nextHopMTU));
                }

                if (icmpLayer->isMessageOfType(pcpp::ICMP_SOURCE_QUENCH) || icmpLayer->isMessageOfType(pcpp::ICMP_REDIRECT) || icmpLayer->isMessageOfType(pcpp::ICMP_TIME_EXCEEDED))
                {
                    // check this works
                    addChild(layerRoot, QString("Unused: "), QString::number(icmpLayer->getSourceQuenchdata()->unused));
                }

                if (icmpLayer->isMessageOfType(pcpp::ICMP_ROUTER_ADV))
                {

                    addChild(layerRoot, QString("AddressEntrySize: "), QString::number(icmpLayer->getRouterAdvertisementData()->header->addressEntrySize));
                    addChild(layerRoot, QString("Advertisement Count: "), QString::number(icmpLayer->getRouterAdvertisementData()->header->advertisementCount));
                    addChild(layerRoot, QString("Lifetime: "), QString::number(icmpLayer->getRouterAdvertisementData()->header->lifetime));

                    for (int i = 0; i < icmpLayer->getRouterAdvertisementData()->header->addressEntrySize; i++)
                    {
                        auto *routerItem = new QTreeWidgetItem(layerRoot);
                        addSubRoot(layerRoot, routerItem, QString("Router " + i));

                        addChild(routerItem, QString("Router Address: "), QString::fromStdString(icmpLayer->getRouterAdvertisementData()->getRouterAddress(i)->getAddress().toString()));
                        addChild(routerItem, QString("Preference Level: "), QString::number(icmpLayer->getRouterAdvertisementData()->getRouterAddress(i)->preferenceLevel));
                    }
                }

                if (icmpLayer->isMessageOfType(pcpp::ICMP_PARAM_PROBLEM))
                {
                    addChild(layerRoot, QString("Byte offset: "), QString::number(icmpLayer->getParamProblemData()->pointer));
                    addChild(layerRoot, QString("Unused 1 byte: "), QString::number(icmpLayer->getParamProblemData()->unused1));
                    addChild(layerRoot, QString("Unused 2 byte: "), QString::number(icmpLayer->getParamProblemData()->unused2));
                }

                if (icmpLayer->isMessageOfType(pcpp::ICMP_ADDRESS_MASK_REQUEST) || icmpLayer->isMessageOfType(pcpp::ICMP_ADDRESS_MASK_REPLY))
                {
                    addChild(layerRoot, QString("Mask request Identifier: "), QString::number(icmpLayer->getAddressMaskRequestData()->id));
                    addChild(layerRoot, QString("Mask request Sequence Number: "), QString::number(icmpLayer->getAddressMaskRequestData()->sequence));
                    addChild(layerRoot, QString("Subnet mask of requesting host: "), QString::number(icmpLayer->getAddressMaskRequestData()->addressMask));
                }

                if (icmpLayer->isMessageOfType(pcpp::ICMP_INFO_REQUEST) || icmpLayer->isMessageOfType(pcpp::ICMP_INFO_REPLY))
                {
                    addChild(layerRoot, QString("Information request Identifier: "), QString::number(icmpLayer->getInfoRequestData()->id));
                    addChild(layerRoot, QString("Information request Sequence Number: "), QString::number(icmpLayer->getInfoRequestData()->sequence));
                }
            }
        }
    }

    std::string getICMPType(pcpp::IcmpMessageType type)
    {
        switch (type)
        {
        /** ICMP echo (ping) reply message */
        case pcpp::ICMP_ECHO_REPLY:
            return "Echo (ping) reply";
            /** ICMP destination unreachable message */
        case pcpp::ICMP_DEST_UNREACHABLE:
            return "Destination unreachable";
            /** ICMP source quench message */
        case pcpp::ICMP_SOURCE_QUENCH:
            return "Source quench";
            /** ICMP redirect message */
        case pcpp::ICMP_REDIRECT:
            return "Redirect";
            /** ICMP echo (ping) request message */
        case pcpp::ICMP_ECHO_REQUEST:
            return "Echo (ping) request";
            /** ICMP router advertisement message */
        case pcpp::ICMP_ROUTER_ADV:
            return "Router advertisement";
            /** ICMP router soliciatation message */
        case pcpp::ICMP_ROUTER_SOL:
            return "";
            /** ICMP time-to-live excceded message */
        case pcpp::ICMP_TIME_EXCEEDED:
            return "Time-to-live excceded";
            /** ICMP parameter problem message */
        case pcpp::ICMP_PARAM_PROBLEM:
            return "Paramenter problem";
            /** ICMP timestamp request message */
        case pcpp::ICMP_TIMESTAMP_REQUEST:
            return "Timestap request ";
            /** ICMP timestamp reply message */
        case pcpp::ICMP_TIMESTAMP_REPLY:
            return "Timestap reply";
            /** ICMP information request message */
        case pcpp::ICMP_INFO_REQUEST:
            return "Information request";
            /** ICMP information reply message */
        case pcpp::ICMP_INFO_REPLY:
            return "Information reply";
            /** ICMP address mask request message */
        case pcpp::ICMP_ADDRESS_MASK_REQUEST:
            return "Addres mask request";
            /** ICMP address mask reply message */
        case pcpp::ICMP_ADDRESS_MASK_REPLY:
            return "Address mask reply";
            /** ICMP message type unsupported by PcapPlusPlus */
        case pcpp::ICMP_UNSUPPORTED:
            return "Unsupported";
        default:
            return "Unknown";
        }
    }
}