#include "packet_layer_link.h"
namespace pkl
{
    void setEthernetData(const pcpp::Packet &packet, QTreeWidget *packetInfo)
    {
        if (packet.isPacketOfType(pcpp::Ethernet))
        {
            pcpp::EthLayer const *ethernetLayer = packet.getLayerOfType<pcpp::EthLayer>();
            if (ethernetLayer == nullptr)
            {
                qDebug() << "Something went wrong, couldn't find Ethernet layer";
            }
            else
            {
                auto *layerRoot = new QTreeWidgetItem(packetInfo);

                addRoot(packetInfo, layerRoot, QString::fromStdString("Ethernet II"));
                addChild(layerRoot, QString("Source: "), QString::fromStdString(ethernetLayer->getSourceMac().toString()));
                addChild(layerRoot, QString("Destination: "), QString::fromStdString(ethernetLayer->getDestMac().toString()));
                addChild(layerRoot, QString("Type: "), QString::number(ethernetLayer->getEthHeader()->etherType));
            }
        }
    }

    void setEthernetDot3Data(const pcpp::Packet &packet, QTreeWidget *packetInfo)
    {
        if (packet.isPacketOfType(pcpp::EthernetDot3))
        {
            pcpp::EthDot3Layer const *ethernetLayer = packet.getLayerOfType<pcpp::EthDot3Layer>();
            if (ethernetLayer == nullptr)
            {
                qDebug() << "Something went wrong, couldn't find IEEE 802.3 Ethernet layer";
            }
            else
            {
                auto *layerRoot = new QTreeWidgetItem(packetInfo);

                addRoot(packetInfo, layerRoot, QString::fromStdString("IEEE 802.3 Ethernet"));
                addChild(layerRoot, QString("Source: "), QString::fromStdString(ethernetLayer->getSourceMac().toString()));
                addChild(layerRoot, QString("Destination: "), QString::fromStdString(ethernetLayer->getDestMac().toString()));
                addChild(layerRoot, QString("Length: "), QString::number(ethernetLayer->getHeaderLen()));
            }
        }
    }

    pcpp::LinkLayerType getLinkLayerType(const QString &LinkLayerName)
    {
        QHashIterator<pcpp::LinkLayerType, QString> iter(linkTypeName);
        while (iter.hasNext())
        {
            iter.next();
            qDebug() << iter.key() << " : " << iter.value();
            if (iter.value() == LinkLayerName)
            {
                return iter.key();
            }
        }
        return pcpp::LINKTYPE_ETHERNET;
    }

    QString getLinkLayerName(pcpp::LinkLayerType LinkLayerType)
    {
        if (linkTypeName.contains(LinkLayerType))
        {
            return linkTypeName.value(LinkLayerType);
        }
        return nullptr;
    }
}