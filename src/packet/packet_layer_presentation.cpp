#include "packet_layer_presentation.h"

/**
 * \namespace pkl
 * \brief Namespace for network packet layer functions
 *
 */
namespace pkl
{
    void setTLSData(const pcpp::Packet &packet, QTreeWidget *packetInfo)
    {
        if (packet.isPacketOfType(pcpp::SSL))
        {
            pcpp::SSLLayer *tlsLayer = packet.getLayerOfType<pcpp::SSLLayer>();
            if (tlsLayer == nullptr)
            {
                qDebug() << "Something went wrong, couldn't find SSL/TLS layer";
            }
            else
            {
                auto *layerRoot = new QTreeWidgetItem(packetInfo);
                addRoot(packetInfo, layerRoot, QString("Transport Layer Security"));

                while (tlsLayer != nullptr)
                {
                    std::string contentType = getTLSRecordType(tlsLayer->getRecordType());
                    std::string version = tlsLayer->getRecordVersion().toString();
                    std::string versionInt = std::to_string(tlsLayer->getRecordVersion().asUInt());
                    std::string length = std::to_string(tlsLayer->getDataLen());

                    auto *record = new QTreeWidgetItem(layerRoot);
                    addSubRoot(layerRoot, record, QString::fromStdString(version + " Record Layer: " + contentType));
                    addChild(record, QString("Content Type: "), QString::fromStdString(contentType));
                    addChild(record, QString("Version: "), QString::fromStdString(version + " " + versionInt));
                    addChild(record, QString("Length: "), QString::fromStdString(length));

                    pcpp::SSLRecordType recType = tlsLayer->getRecordType();

                    /* SSL handshake message */
                    if (recType == pcpp::SSL_HANDSHAKE)
                    {
                        auto *handshakeLayer = dynamic_cast<pcpp::SSLHandshakeLayer *>(tlsLayer);
                        int handshakeCount = handshakeLayer->getHandshakeMessagesCount();
                        for (int i = 0; i < handshakeCount; i++)
                        {
                            pcpp::SSLHandshakeMessage const *message = handshakeLayer->getHandshakeMessageAt(i);

                            std::string handType = getTLSHandshakeType(message->getHandshakeType());
                            std::string handLength = std::to_string(message->getMessageLength());

                            auto *handshakeItem = new QTreeWidgetItem(record);
                            addSubRoot(record, handshakeItem, QString::fromStdString("Handshake Protocol: " + handType));

                            addChild(handshakeItem, QString("Handshake Type: "), QString::fromStdString(handType));
                            addChild(handshakeItem, QString("Length: "), QString::fromStdString(handLength));

                            getTLSHandshakeHelloClientData(handshakeLayer, handshakeItem);
                            getTLSHandshakeServerClientData(handshakeLayer, handshakeItem);
                            // getTLSHandshakeCertificate(handshakeLayer);
                        }
                    }
                    else if (recType == pcpp::SSL_ALERT)
                    {
                        auto *alertLayer = dynamic_cast<pcpp::SSLAlertLayer *>(tlsLayer);
                        std::string alertLevel = getTLSAlertLevel(alertLayer->getAlertLevel());
                        std::string alertDescription = getTLSAlertDescription(alertLayer->getAlertDescription());

                        addChild(record, QString("Alert Level: "), QString::fromStdString(alertLevel));
                        addChild(record, QString("Alert Message: "), QString::fromStdString(alertDescription));
                    }
                    else if (recType == pcpp::SSL_CHANGE_CIPHER_SPEC)
                    {
                        auto const *cipherLayer = dynamic_cast<pcpp::SSLChangeCipherSpecLayer *>(tlsLayer);
                        std::string cipherType = cipherLayer->toString();

                        auto *cipherItem = new QTreeWidgetItem(record);
                        addSubRoot(record, cipherItem, QString::fromStdString(cipherType));
                    }

                    // get next layer
                    tlsLayer = packet.getNextLayerOfType<pcpp::SSLLayer>(tlsLayer);
                }
            }
        }
    }

    static void getTLSServerKeyExchangeMessage(pcpp::SSLHandshakeLayer const *handshakeLayer,
                                               QTreeWidgetItem const *handshakeItem)
    {
        pcpp::SSLServerKeyExchangeMessage const *serverKEMessage =
            handshakeLayer->getHandshakeMessageOfType<pcpp::SSLServerKeyExchangeMessage>();

        if (serverKEMessage == nullptr)
        {
            return;
        }
    }

    static void getTLSHandshakeCertificate(const pcpp::SSLHandshakeLayer *handshakeLayer,
                                           const QTreeWidgetItem *handshakeItem)
    {
        pcpp::SSLCertificateMessage const *certificateMessage =
            handshakeLayer->getHandshakeMessageOfType<pcpp::SSLCertificateMessage>();

        if (certificateMessage == nullptr)
        {
            return;
        }

        for (int i = 0; i < certificateMessage->getNumOfCertificates(); i++)
        {
            auto certMessage = certificateMessage->getCertificate(i);
            std::string certLength = std::to_string(certMessage->getDataLength());
        }
    }

    static void getTLSHandshakeServerClientData(const pcpp::SSLHandshakeLayer *handshakeLayer,
                                                QTreeWidgetItem *handshakeItem)
    {
        pcpp::SSLServerHelloMessage const *serverHelloMessage =
            handshakeLayer->getHandshakeMessageOfType<pcpp::SSLServerHelloMessage>();

        if (serverHelloMessage == nullptr)
        {
            return;
        }

        std::string handshakeVersion = serverHelloMessage->getHandshakeVersion().toString();
        // std::string handshakeRandom(serverHelloMessage->getClientHelloHeader()->random, 32);
        std::string handshakeSessionIDLength = std::to_string(serverHelloMessage->getSessionIDLength());
        // std::string handshakeSessionID(reinterpret_cast<char const *>(serverHelloMessage->getSessionID(), serverHelloMessage->getSessionIDLength()));

        std::string handshakeCipherSuite = serverHelloMessage->getCipherSuite()->asString();

        std::string handshakeCompressionMethods = std::to_string(serverHelloMessage->getCompressionMethodsValue());
        std::string handshakeExtLength = std::to_string(serverHelloMessage->getExtensionsLength());

        addChild(handshakeItem, QString("Handshake Type: "), QString::fromStdString(handshakeVersion));
        addChild(handshakeItem, QString("Session ID: "), QString::fromStdString(handshakeSessionIDLength));
        addChild(handshakeItem, QString("Cipher Suite: "), QString::fromStdString(handshakeCipherSuite));
        addChild(handshakeItem, QString("Compression Method: "), QString::fromStdString(handshakeCompressionMethods));
        addChild(handshakeItem, QString("Extensions Length: "), QString::fromStdString(handshakeExtLength));

        /* handshake cipher extension */
        for (int j = 0; j < serverHelloMessage->getExtensionCount(); j++)
        {
            auto handshakeExt = serverHelloMessage->getExtension(j);
            std::string handshakeExtName = getTLSHandshakeExtensionType(handshakeExt->getType());
            std::string handshakeLength = std::to_string(handshakeExt->getLength());
            /* not displaying extension data */
        }
        addChild(handshakeItem, QString(""), QString::fromStdString(serverHelloMessage->generateTLSFingerprint().toString()));
        addChild(handshakeItem, QString::fromStdString(serverHelloMessage->generateTLSFingerprint().toMD5()), QString(""));
    }

    static void getTLSHandshakeHelloClientData(const pcpp::SSLHandshakeLayer *handshakeLayer,
                                               QTreeWidgetItem *handshakeItem)
    {
        // try to find client-hello message
        pcpp::SSLClientHelloMessage const *clientHelloMessage =
            handshakeLayer->getHandshakeMessageOfType<pcpp::SSLClientHelloMessage>();

        if (clientHelloMessage == nullptr)
        {
            return;
        }

        std::string handshakeVersion = clientHelloMessage->getHandshakeVersion().toString();
        // std::string handshakeRandom(clientHelloMessage->getClientHelloHeader()->random, 32);
        std::string handshakeSessionIDLength = std::to_string(clientHelloMessage->getSessionIDLength());
        // std::string handshakeSessionID(reinterpret_cast<char const *>(clientHelloMessage->getSessionID(), clientHelloMessage->getSessionIDLength()));
        std::string handshakeCipherSuiteCount = std::to_string(clientHelloMessage->getCipherSuiteCount());

        addChild(handshakeItem, QString("Version: "), QString::fromStdString(handshakeVersion));
        // addChild(handshakeItem, QString("Random: "), QString::fromStdString(handshakeVersion));
        addChild(handshakeItem, QString("Session ID Length: "), QString::fromStdString(handshakeSessionIDLength));
        // addChild(handshakeItem, QString("Session ID: "), QString::fromStdString(handshakeSessionID));
        addChild(handshakeItem, QString("Cipher Suite Count: "), QString::fromStdString(handshakeCipherSuiteCount));

        QTreeWidgetItem *cipherSuite = new QTreeWidgetItem(handshakeItem);
        addSubRoot(handshakeItem, cipherSuite, QString::fromStdString("Cipher Suites: " + handshakeCipherSuiteCount));

        // /* handshake cipher suites */
        // for (int j = 0; j < clientHelloMessage->getCipherSuiteCount(); j++)
        // {
        //     auto handshakeCipher = clientHelloMessage->getCipherSuite(j);
        //     std::string cipherName = handshakeCipher->asString();
        //     std::string cipherID = std::to_string(handshakeCipher->getID());
        //     addChild(cipherSuite, QString("Cipher Suite"), QString::fromStdString(cipherName + " " + cipherID));
        // }

        std::string handshakeCompressionMethods = std::to_string(clientHelloMessage->getCompressionMethodsValue());
        std::string handshakeExtLength = std::to_string(clientHelloMessage->getExtensionsLength());

        addChild(handshakeItem, QString("Compression Methods: "), QString::fromStdString(handshakeCompressionMethods));
        addChild(handshakeItem, QString("Extensions Length: "), QString::fromStdString(handshakeExtLength));

        /* handshake cipher extension */
        for (int j = 0; j < clientHelloMessage->getExtensionCount(); j++)
        {
            auto handshakeExt = clientHelloMessage->getExtension(j);
            std::string handshakeExtName = getTLSHandshakeExtensionType(handshakeExt->getType());
            std::string handshakeLength = std::to_string(handshakeExt->getTotalLength());

            QTreeWidgetItem *extension = new QTreeWidgetItem(handshakeItem);
            addSubRoot(handshakeItem, extension, QString::fromStdString("Extension: " + handshakeExtName));

            addChild(extension, QString("Type: "), QString::fromStdString(handshakeExtName));
            addChild(extension, QString("Length: "), QString::fromStdString(handshakeExtLength));

            /* not displaying extension data */
        }

        addChild(handshakeItem, QString(""), QString::fromStdString(clientHelloMessage->generateTLSFingerprint().toString()));
        addChild(handshakeItem, QString::fromStdString(clientHelloMessage->generateTLSFingerprint().toMD5()), QString(""));
    }

    static std::string getTLSRecordType(pcpp::SSLRecordType recordType)
    {
        switch (recordType)
        {
            /* Change-cipher-spec message */
        case pcpp::SSL_CHANGE_CIPHER_SPEC:
            return "Change Cipher Spec";
            /* SSL alert message */
        case pcpp::SSL_ALERT:
            return "Alert";
            /* SSL handshake message */
        case pcpp::SSL_HANDSHAKE:
            return "Handshake";
            /* SSL data message */
        case pcpp::SSL_APPLICATION_DATA:
            return "Application Data";
        default:
            return "Unknown";
        }
    }

    static std::string getTLSHandshakeType(pcpp::SSLHandshakeType handshakeType)
    {

        switch (handshakeType)
        {
        /* Hello-request message type */
        case pcpp::SSL_HELLO_REQUEST:
            return "Hello Request";
        /* Client-hello message type */
        case pcpp::SSL_CLIENT_HELLO:
            return "Client Hello";
        /* Server-hello message type */
        case pcpp::SSL_SERVER_HELLO:
            return "Server Hello";
        /* New-session-ticket message type */
        case pcpp::SSL_NEW_SESSION_TICKET:
            return "New Session Ticket";
        /* End-of-early-data message type (TLS 1.3) */
        case pcpp::SSL_END_OF_EARLY_DATE:
            return "End-of-early-data";
        /* Encrypted-extensions message type (TLS 1.3) */
        case pcpp::SSL_ENCRYPTED_EXTENSIONS:
            return "Encrypted-extensions";
        /* Certificate message type */
        case pcpp::SSL_CERTIFICATE:
            return "Certificate";
        /* Server-key-exchange message type */
        case pcpp::SSL_SERVER_KEY_EXCHANGE:
            return "Server-key-exchange";
        /* Certificate-request message type */
        case pcpp::SSL_CERTIFICATE_REQUEST:
            return "Certificate-request";
        /* Server-hello-done message type */
        case pcpp::SSL_SERVER_DONE:
            return "Server-hello-done";
        /* Certificate-verify message type */
        case pcpp::SSL_CERTIFICATE_VERIFY:
            return "Certificate-verify";
        /* Client-key-exchange message type */
        case pcpp::SSL_CLIENT_KEY_EXCHANGE:
            return "Client-key-exchange";
        /* Finish message type */
        case pcpp::SSL_FINISHED:
            return "Finish";
        /* Key-update message type (TLS 1.3) */
        case pcpp::SSL_KEY_UPDATE:
            return "Key-update";
        /* Unknown SSL handshake message */
        case pcpp::SSL_HANDSHAKE_UNKNOWN:
            return "Unknown";
        default:
            return "Unknown";
        }
    }

    static std::string getTLSHandshakeExtensionType(pcpp::SSLExtensionType extType)
    {
        switch (extType)
        {
        /* Server Name Indication extension */
        case pcpp::SSL_EXT_SERVER_NAME:
            return "Server Name";
        /* Maximum Fragment Length Negotiation extension */
        case pcpp::SSL_EXT_MAX_FRAGMENT_LENGTH:
            return "Maximum Fragment Length";
        /* Client Certificate URLs extension */
        case pcpp::SSL_EXT_CLIENT_CERTIFICATE_URL:
            return "Client Certificate URLs";
        /* Trusted CA Indication extension */
        case pcpp::SSL_EXT_TRUSTED_CA_KEYS:
            return "Trusted CA";
        /* Truncated HMAC extension */
        case pcpp::SSL_EXT_TRUNCATED_HMAC:
            return "Truncated HMAC";
        /* Certificate Status Request extension */
        case pcpp::SSL_EXT_STATUS_REQUEST:
            return "Certificate Status Request";
        /* TLS User Mapping extension */
        case pcpp::SSL_EXT_USER_MAPPING:
            return "TLS User Mapping";
        /* Client Authorization extension */
        case pcpp::SSL_EXT_CLIENT_AUTHZ:
            return "Client Authorization";
        /* Server Authorization extension */
        case pcpp::SSL_EXT_SERVER_AUTHZ:
            return "Server Authorization";
        /* Certificate Type extension */
        case pcpp::SSL_EXT_CERT_TYPE:
            return "Certificate Type";
        /* Supported Groups extension (renamed from "elliptic curves") */
        case pcpp::SSL_EXT_SUPPORTED_GROUPS:
            return "Supported Groups extension";
        /* Elliptic Curves Point Format extension */
        case pcpp::SSL_EXT_EC_POINT_FORMATS:
            return "Elliptic Curves Point Format";
        /* Secure Remote Password extension */
        case pcpp::SSL_EXT_SRP:
            return "Secure Remote Password";
        /* Signature Algorithms extension */
        case pcpp::SSL_EXT_SIGNATURE_ALGORITHMS:
            return "Signature Algorithms";
        /* Use Secure Real-time Transport Protocol extension */
        case pcpp::SSL_EXT_USE_SRTP:
            return "Secure Real-time Transport Protocol";
        /* TLS Heartbit extension */
        case pcpp::SSL_EXT_HEARTBEAT:
            return "TLS Heartbit";
        /* Application Layer Protocol Negotiation (ALPN) extension */
        case pcpp::SSL_EXT_APPLICATION_LAYER_PROTOCOL_NEGOTIATION:
            return "Application Layer Protocol Negotiation (ALPN)";
        /* Status Request extension */
        case pcpp::SSL_EXT_STATUS_REQUEST_V2:
            return "Status Request";
        /* Signed Certificate Timestamp extension */
        case pcpp::SSL_EXT_SIGNED_CERTIFICATE_TIMESTAMP:
            return "Signed Certificate Timestamp";
        /* Client Certificate Type extension */
        case pcpp::SSL_EXT_CLIENT_CERTIFICATE_TYPE:
            return "Client Certificate Type";
        /* Server Certificate Type extension */
        case pcpp::SSL_EXT_SERVER_CERTIFICATE_TYPE:
            return "Server Certificate Type";
        /* ClientHello Padding extension */
        case pcpp::SSL_EXT_PADDING:
            return "ClientHello Padding";
        /* Encrypt-then-MAC extension */
        case pcpp::SSL_EXT_ENCRYPT_THEN_MAC:
            return "Encrypt-then-MAC";
        /* Extended Master Secret extension */
        case pcpp::SSL_EXT_EXTENDED_MASTER_SECRET:
            return "Extended Master Secret";
        /* Token Binding extension */
        case pcpp::SSL_EXT_TOKEN_BINDING:
            return "Token Binding";
        /* SessionTicket TLS extension */
        case pcpp::SSL_EXT_SESSIONTICKET_TLS:
            return "SessionTicket TLS";
        /* Pre-shared key (PSK) extension (TLS 1.3) */
        case pcpp::SSL_EXT_PRE_SHARED_KEY:
            return "Pre-shared key (PSK)";
        /* Early data extension (TLS 1.3) */
        case pcpp::SSL_EXT_EARLY_DATA:
            return "Early data";
        /* Supported versions extension (TLS 1.3) */
        case pcpp::SSL_EXT_SUPPORTED_VERSIONS:
            return "Supported versions";
        /* Cookie extension (TLS 1.3) */
        case pcpp::SSL_EXT_COOKIE:
            return "Cookie";
        /* Pre-Shared Key Exchange Modes extension (TLS 1.3) */
        case pcpp::SSL_EXT_PSK_KEY_EXCHANGE_MODES:
            return "Pre-Shared Key Exchange Modes";
        /* Certificate authorities extension (TLS 1.3) */
        case pcpp::SSL_EXT_CERTIFICATE_AUTHORITIES:
            return "Certificate authorities";
        /* Old filters extension (TLS 1.3) */
        case pcpp::SSL_EXT_OLD_FILTERS:
            return "Old filters";
        /* Post handshake auth extension (TLS 1.3) */
        case pcpp::SSL_EXT_POST_HANDSHAKE_AUTH:
            return "Post handshake auth";
        /* Signature algorithm cert extension (TLS 1.3) */
        case pcpp::SSL_EXT_SIGNATURE_ALGORITHM_CERT:
            return "Signature algorithm cert";
        /* Key share extension (TLS 1.3) */
        case pcpp::SSL_EXT_KEY_SHARE:
            return "Key share";
        /* Renegotiation Indication extension */
        case pcpp::SSL_EXT_RENEGOTIATION_INFO:
            return "Renegotiation Indication";
        /* Unknown extension */
        case pcpp::SSL_EXT_Unknown:
            return "Unknown";
        default:
            return "Unknown";
        };
    }

    static std::string getTLSAlertLevel(pcpp::SSLAlertLevel level)
    {
        switch (level)
        {
        /** Warning level alert */
        case pcpp::SSL_ALERT_LEVEL_WARNING:
            return "Warning ";
            /** Fatal level alert */
        case pcpp::SSL_ALERT_LEVEL_FATAL:
            return "Fatal";
            /** For encrypted alerts the level is unknown so this type will be returned */
        case pcpp::SSL_ALERT_LEVEL_ENCRYPTED:
            return "Encypted";
        default:
            return "Unknown";
        }
    }

    static std::string getTLSAlertDescription(pcpp::SSLAlertDescription description)
    {
        switch (description)
        {
        /** Close notify alert */
        case pcpp::SSL_ALERT_CLOSE_NOTIFY:
            return "Close notify";
            /** Unexpected message alert */
        case pcpp::SSL_ALERT_UNEXPECTED_MESSAGE:
            return "Unexpected message";
            /** Bad record MAC alert */
        case pcpp::SSL_ALERT_BAD_RECORD_MAC:
            return "Bad record MAC";
            /** Decryption failed alert */
        case pcpp::SSL_ALERT_DECRYPTION_FAILED:
            return "Description failed";
            /**  */
        case pcpp::SSL_ALERT_RECORD_OVERFLOW:
            return "Record Overflow";
            /** Decompression failure alert */
        case pcpp::SSL_ALERT_DECOMPRESSION_FAILURE:
            return "Decompression falure";
            /** Handshake failure alert */
        case pcpp::SSL_ALERT_HANDSHAKE_FAILURE:
            return "Handshake failure";
            /** No certificate alert */
        case pcpp::SSL_ALERT_NO_CERTIFICATE:
            return "No certificate";
            /** Bad certificate alert */
        case pcpp::SSL_ALERT_BAD_CERTIFICATE:
            return "Bad certificate";
            /** Unsupported certificate */
        case pcpp::SSL_ALERT_UNSUPPORTED_CERTIFICATE:
            return "Unsupported certificate";
            /** Certificate revoked alert */
        case pcpp::SSL_ALERT_CERTIFICATE_REVOKED:
            return "Certificate revoked";
            /** Certificate expired alert */
        case pcpp::SSL_ALERT_CERTIFICATE_EXPIRED:
            return "Certificate expired";
            /** Certificate unknown alert */
        case pcpp::SSL_ALERT_CERTIFICATE_UNKNOWN:
            return "Certificate unknown";
            /** Illegal parameter alert */
        case pcpp::SSL_ALERT_ILLEGAL_PARAMETER:
            return "Illegal parameter";
            /** Unknown CA alert */
        case pcpp::SSL_ALERT_UNKNOWN_CA:
            return "CA";
            /** Access denied alert */
        case pcpp::SSL_ALERT_ACCESS_DENIED:
            return "Access denied";
            /** Decode error alert */
        case pcpp::SSL_ALERT_DECODE_ERROR:
            return "Decode error";
            /** Decrypt error alert */
        case pcpp::SSL_ALERT_DECRYPT_ERROR:
            return "Decypt error";
            /** Export restriction alert */
        case pcpp::SSL_ALERT_EXPORT_RESTRICTION:
            return "Export restriction";
            /** Protocol version alert */
        case pcpp::SSL_ALERT_PROTOCOL_VERSION:
            return "Protocol Version";
            /** Insufficient security alert */
        case pcpp::SSL_ALERT_INSUFFICIENT_SECURITY:
            return "Insufficient security";
            /** Internal error alert */
        case pcpp::SSL_ALERT_INTERNAL_ERROR:
            return "Internal error";
            /** User cancelled alert */
        case pcpp::SSL_ALERT_USER_CANCELLED:
            return "User cancelled";
            /** No negotiation alert */
        case pcpp::SSL_ALERT_NO_RENEGOTIATION:
            return "No renegotiation";
            /** Unsupported extension alert */
        case pcpp::SSL_ALERT_UNSUPPORTED_EXTENSION:
            return "Unsupportted extension";
            /** Encrtpyed alert (cannot determine its type) */
        case pcpp::SSL_ALERT_ENCRYPTED:
            return "Encypted";
        default:
            return "Alert unknown";
        }
    }
}