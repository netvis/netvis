#include "utils_app_settings.h"

namespace AppSetting
{

    const QString geometryKey = "geometry";

    const QString FileDefaultsGroupKey = "FileDefaultsGroup";

    const QString defaultSaveFileNameKey = "defaultSaveFileName";
    const QString defaultSaveFileName = "savefile";
    const QString defaultFileFormatKey = "defaultFileFormat";
    const QString defaultFileCompressionKey = "defaultFileCompression";

    const QString defaultSaveFolderKey = "defaultSaveFolder";
    const QString lastSaveFolderKey = "lastSavedFileFolder";
    const QString applicationRootFolderKey = "applicationRootFolder";

    const QString defaultLogFileNameKey = "logFile";

    const QString recentFileListGroupKey = "recentFileListGroup";
    const QString recentFilesKey = "file";

    const QString recentFilterListGroupKey = "recentFilterListGroup";
    const QString recentFilterKey = "filter";

    QByteArray getGeometry()
    {
        QSettings settings(QCoreApplication::organizationName(),
                           QCoreApplication::applicationName());
        return settings.value(geometryKey, QByteArray()).toByteArray();
    }

    void setGeometry(const QByteArray &appGeometry)
    {
        QSettings settings(QCoreApplication::organizationName(),
                           QCoreApplication::applicationName());
        settings.setValue(geometryKey, appGeometry);
    }

    QString getDefaultSaveFolder()
    {
        QSettings settings(QCoreApplication::organizationName(),
                           QCoreApplication::applicationName());
        settings.beginGroup(FileDefaultsGroupKey);
        /* read/initialise defaultsavefolder */
        QString defaultSaveFolderValue;
        if (settings.contains(defaultSaveFolderKey))
        {
            defaultSaveFolderValue = settings.value(defaultSaveFolderKey).toString().trimmed();
            if (defaultSaveFolderValue.isEmpty())
            {
                defaultSaveFolderValue = QDir::homePath() + "/" +
                                         QCoreApplication::applicationName() + "/";
                AppSetting::setDefaultSaveFolder(defaultSaveFolderValue);
            }
        }
        else
        {
            defaultSaveFolderValue = QDir::homePath() + "/" +
                                     QCoreApplication::applicationName() + "/";
            AppSetting::setDefaultSaveFolder(defaultSaveFolderValue);
        }
        settings.endGroup();
        return defaultSaveFolderValue;
    }

    void setDefaultSaveFolder(const QString &folderName)
    {
        QSettings settings(QCoreApplication::organizationName(),
                           QCoreApplication::applicationName());
        settings.beginGroup(FileDefaultsGroupKey);
        settings.setValue(defaultSaveFolderKey, folderName);
        settings.endGroup();
    }

    QString getDefaultSaveFileName()
    {
        QSettings settings(QCoreApplication::organizationName(),
                           QCoreApplication::applicationName());
        settings.beginGroup(FileDefaultsGroupKey);
        QString defaultSaveFileNameValue;
        /* read/initialise lastsavefolder */
        if (settings.contains(defaultSaveFileNameKey))
        {
            defaultSaveFileNameValue = settings.value(defaultSaveFileNameKey).toString().trimmed();
            if (defaultSaveFileNameValue.isEmpty())

            {
                defaultSaveFileNameValue = defaultSaveFileName;
                AppSetting::setDefaultSaveFileName(defaultSaveFileNameValue);
            }
        }
        else
        {
            defaultSaveFileNameValue = defaultSaveFileName;
            AppSetting::setDefaultSaveFileName(defaultSaveFileNameValue);
        }
        settings.endGroup();
        return defaultSaveFileNameValue;
    }

    void setDefaultSaveFileName(const QString &fileName)
    {
        QSettings settings(QCoreApplication::organizationName(),
                           QCoreApplication::applicationName());
        settings.beginGroup(FileDefaultsGroupKey);
        settings.setValue(defaultSaveFileNameKey, fileName);
        settings.endGroup();
    }

    void setDefaultFileFormat(const cmm::fileFormat &fileFormat)
    {
        QSettings settings(QCoreApplication::organizationName(),
                           QCoreApplication::applicationName());
        settings.beginGroup(FileDefaultsGroupKey);
        settings.setValue(defaultFileFormatKey, cmm::getFileExtension(fileFormat));
        settings.endGroup();
    }

    cmm::fileFormat getDefaultFileFormat()
    {
        QSettings settings(QCoreApplication::organizationName(),
                           QCoreApplication::applicationName());
        settings.beginGroup(FileDefaultsGroupKey);
        cmm::fileFormat defaultFileFormatValue = cmm::pcapng;
        QString fileType;
        /* read/initialise lastsavefolder */
        if (settings.contains(defaultFileFormatKey))
        {
            fileType = settings.value(defaultFileFormatKey).toString().trimmed();
            if (fileType.isEmpty())
            {
                AppSetting::setDefaultFileFormat(defaultFileFormatValue);
            }
            else
            {
                defaultFileFormatValue = cmm::getFileFormat(fileType);
            }
        }
        else
        {
            defaultFileFormatValue = cmm::getFileFormat(fileType);
            AppSetting::setDefaultFileFormat(defaultFileFormatValue);
        }
        settings.endGroup();
        return defaultFileFormatValue;
    }

    bool getDefaultFileCompression()
    {
        QSettings settings(QCoreApplication::organizationName(),
                           QCoreApplication::applicationName());
        settings.beginGroup(FileDefaultsGroupKey);
        QString fileCompression;
        bool isCompression = false;
        /* read/initialise lastsavefolder */
        if (settings.contains(defaultFileCompressionKey))
        {
            fileCompression = settings.value(defaultFileCompressionKey).toString().trimmed().toLower();
            if (fileCompression.isEmpty())
            {
                AppSetting::setDefaultFileCompression("false");
            }
            else
            {
                if (fileCompression == "true")
                    isCompression = true;
            }
        }
        else
        {
            AppSetting::setDefaultFileCompression(isCompression);
        }
        settings.endGroup();
        return isCompression;
    }

    void setDefaultFileCompression(const bool &isCompression)
    {
        QSettings settings(QCoreApplication::organizationName(),
                           QCoreApplication::applicationName());
        settings.beginGroup(FileDefaultsGroupKey);
        if (isCompression)
            settings.setValue(defaultFileFormatKey, "true");
        else
            settings.setValue(defaultFileFormatKey, "false");
        settings.endGroup();
    }

    QString getLastSaveFolder()
    {
        QSettings settings(QCoreApplication::organizationName(),
                           QCoreApplication::applicationName());
        settings.beginGroup(FileDefaultsGroupKey);
        QString lastSaveFolderValue;
        /* read/initialise lastsavefolder */
        if (settings.contains(lastSaveFolderKey))
        {
            lastSaveFolderValue = settings.value(lastSaveFolderKey).toString().trimmed();
            if (lastSaveFolderValue.isEmpty())
            {
                lastSaveFolderValue = AppSetting::getDefaultSaveFolder();
                AppSetting::setLastSaveFolder(lastSaveFolderValue);
            }
        }
        else
        {
            lastSaveFolderValue = AppSetting::getDefaultSaveFolder();
            AppSetting::setLastSaveFolder(lastSaveFolderValue);
        }
        settings.endGroup();
        return lastSaveFolderValue;
    }

    void setLastSaveFolder(const QString &folderName)
    {
        QSettings settings(QCoreApplication::organizationName(),
                           QCoreApplication::applicationName());
        settings.beginGroup(FileDefaultsGroupKey);
        settings.setValue(applicationRootFolderKey, folderName);
        settings.endGroup();
    }

    QString getApplicationRootFolder()
    {
        QSettings settings(QCoreApplication::organizationName(),
                           QCoreApplication::applicationName());
        settings.beginGroup(FileDefaultsGroupKey);
        QString applicationRootFolderValue;
        /* read/initialise lastsavefolder */
        if (settings.contains(applicationRootFolderKey))
        {
            applicationRootFolderValue = settings.value(applicationRootFolderKey).toString().trimmed();
            if (applicationRootFolderValue.isEmpty())

            {
                applicationRootFolderValue = QDir::homePath() + "/" +
                                             QCoreApplication::applicationName() + "/";
                AppSetting::setApplicationRootFolder(applicationRootFolderValue);
            }
        }
        else
        {
            applicationRootFolderValue = QDir::homePath() + "/" +
                                         QCoreApplication::applicationName() + "/";
            AppSetting::setApplicationRootFolder(applicationRootFolderValue);
        }
        settings.endGroup();
        return applicationRootFolderValue;
    }

    void setApplicationRootFolder(const QString &folderName)
    {
        QSettings settings(QCoreApplication::organizationName(),
                           QCoreApplication::applicationName());
        settings.beginGroup(FileDefaultsGroupKey);
        settings.setValue(applicationRootFolderKey, folderName);
        settings.endGroup();
    }

    QString getLogFileName()
    {
        QSettings settings(QCoreApplication::organizationName(),
                           QCoreApplication::applicationName());
        settings.beginGroup(FileDefaultsGroupKey);
        QString logFileNameValue;
        /* read/initialise logfilename */
        if (settings.contains(defaultLogFileNameKey))
        {
            logFileNameValue = settings.value(defaultLogFileNameKey).toString();
            if (!settings.value(defaultLogFileNameKey).toString().isEmpty())
            {

                logFileNameValue = AppSetting::getApplicationRootFolder() +
                                   QCoreApplication::applicationName() + ".log";
                AppSetting::setLogFileName(logFileNameValue);
            }
        }
        else
        {
            logFileNameValue = AppSetting::getApplicationRootFolder() +
                               QCoreApplication::applicationName() + ".log";
            AppSetting::setLogFileName(logFileNameValue);
        }
        settings.endGroup();
        return logFileNameValue;
    }

    void setLogFileName(const QString &fileName)
    {
        QSettings settings(QCoreApplication::organizationName(),
                           QCoreApplication::applicationName());
        settings.beginGroup(FileDefaultsGroupKey);
        settings.setValue(defaultLogFileNameKey, fileName);
        settings.endGroup();
    }

    QStringList getRecentFiles()
    {
        QSettings settings(QCoreApplication::organizationName(),
                           QCoreApplication::applicationName());
        QStringList fileList;
        const int count = settings.beginReadArray(recentFileListGroupKey);
        for (int i = 0; i < count; ++i)
        {
            settings.setArrayIndex(i);
            fileList.append(settings.value(recentFilesKey).toString());
        }
        settings.endArray();
        return fileList;
    }
    void setRecentFiles(const QStringList &fileList)
    {
        QSettings settings(QCoreApplication::organizationName(),
                           QCoreApplication::applicationName());
        const int count = fileList.size();
        settings.beginWriteArray(recentFileListGroupKey);
        for (int i = 0; i < count; ++i)
        {
            settings.setArrayIndex(i);
            settings.setValue(recentFilesKey, fileList.at(i));
        }
        settings.endArray();
    }

    bool isRecentFiles()
    {
        QSettings settings(QCoreApplication::organizationName(),
                           QCoreApplication::applicationName());
        int count = settings.beginReadArray(recentFileListGroupKey);
        settings.endArray();
        return count > 0;
    }

    void prependRecentFiles(const QString &fileName)
    {
        QSettings settings(QCoreApplication::organizationName(),
                           QCoreApplication::applicationName());

        const QStringList oldRecentFilesList = AppSetting::getRecentFiles();
        QStringList recentFilesList = oldRecentFilesList;
        recentFilesList.removeAll(fileName);
        recentFilesList.prepend(fileName);
        if (oldRecentFilesList != recentFilesList)
            AppSetting::setRecentFiles(recentFilesList);
    }

    QStringList getRecentFilterList()
    {
        QSettings settings(QCoreApplication::organizationName(),
                           QCoreApplication::applicationName());
        QStringList filterList;
        const int count = settings.beginReadArray(recentFilterListGroupKey);
        for (int i = 0; i < count; ++i)
        {
            settings.setArrayIndex(i);
            filterList.append(settings.value(recentFilterKey).toString());
        }
        settings.endArray();
        return filterList;
    }

    void setRecentFilterList(const QStringList &filterList)
    {
        QSettings settings(QCoreApplication::organizationName(),
                           QCoreApplication::applicationName());
        const int count = filterList.size();
        settings.beginWriteArray(recentFilterListGroupKey);
        for (int i = 0; i < count; ++i)
        {
            settings.setArrayIndex(i);
            settings.setValue(recentFilterKey, filterList.at(i));
        }
        settings.endArray();
    }

    int isRecentFilter()
    {
        QSettings settings(QCoreApplication::organizationName(),
                           QCoreApplication::applicationName());
        int count = settings.beginReadArray(recentFilterListGroupKey);
        settings.endArray();
        return count > 0;
    }

    void prependRecentFilter(const QString &filter)
    {
        QSettings settings(QCoreApplication::organizationName(),
                           QCoreApplication::applicationName());

        const QStringList oldRecentFilterList = AppSetting::getRecentFilterList();
        QStringList recentFilterList = oldRecentFilterList;
        recentFilterList.removeAll(filter);
        recentFilterList.prepend(filter);
        if (oldRecentFilterList != recentFilterList)
            AppSetting::setRecentFilterList(recentFilterList);
    }
}