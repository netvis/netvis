#include "utils_logger.h"
#include "utils_app_settings.h"

namespace Logger
{

    /* Application logger redirects qDebug output to log file and std:err */
    void messageHandler(QtMsgType type, const QMessageLogContext &context, const QString &message)
    {
        QHash<QtMsgType, QString> msgLevelHash({{QtDebugMsg, "Debug"},
                                                {QtInfoMsg, "Info"},
                                                {QtWarningMsg, "Warning"},
                                                {QtCriticalMsg, "Critical"},
                                                {QtFatalMsg, "Fatal"}});
        QByteArray localMsg = message.toLocal8Bit();
        QTime time = QTime::currentTime();
        QString formattedTime = time.toString("hh:mm:ss.zzz");
        QByteArray formattedTimeMsg = formattedTime.toLocal8Bit();
        QString logLevelName = msgLevelHash[type];
        QByteArray logLevelMsg = logLevelName.toLocal8Bit();
        QString txt = QString("%1 %2: %3 (%4)").arg(formattedTime, logLevelName, message, context.file);

        /* default output to std::err */
        std::cerr << qPrintable(qFormatLogMessage(type, context, message)) << std::endl;

        /* obtain a lock before accessing file to avoid thread clash */
        static QMutex mutex;
        QMutexLocker lock(&mutex);

        // static QFile logFile(QDir::currentPath() + "/" +
        //                      QCoreApplication::applicationName() + "/" +
        //                      QCoreApplication::applicationName() + ".log");
        static QFile logFile(AppSetting::getLogFileName());
        static bool logFileIsOpen = logFile.open(QIODevice::Append | QIODevice::Text);

        if (logFileIsOpen)
        {
            QTextStream ts(&logFile);
            ts << txt << Qt::endl;
            logFile.flush();
        }
        // logFile.close();
    }
}
