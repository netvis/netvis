#include "capture_analyse_handshakes.h"

CaptureAnalyseHandshakes::CaptureAnalyseHandshakes() = default;

CaptureAnalyseHandshakes::~CaptureAnalyseHandshakes() = default;

void CaptureAnalyseHandshakes::startHandShakeAnalysis(int arrowIndex)
{
    index = arrowIndex;
    start();
}

void CaptureAnalyseHandshakes::setPacketPointer(const pcpp::Packet *selectedPacket)
{
    packet = selectedPacket;
}

void CaptureAnalyseHandshakes::run()
{
    analysisComment = nullptr;

    if (packet->isPacketOfType(pcpp::TCP))
    {

        pcpp::TcpLayer const *tcpLayer = packet->getLayerOfType<pcpp::TcpLayer>();

        if (tcpLayer == nullptr)
        {
            qDebug() << "Error: no tcp layer in tcp packet.";
            return;
        }

        /* handshake step 1 */
        if (tcpLayer->getTcpHeader()->synFlag == 1 && tcpLayer->getTcpHeader()->ackFlag == 0)
        {
            // set arrow comments
            analysisComment = "TCP handshake Step 1:\nSyn: " +
                              std::to_string(tcpLayer->getTcpHeader()->sequenceNumber);
            return;
        }

        /* handshake step 2 */
        if (tcpLayer->getTcpHeader()->synFlag == 1 && tcpLayer->getTcpHeader()->ackFlag == 1)
        {
            // set arrow comments
            analysisComment = "TCP handshake Step 2: \nAck: " +
                              std::to_string(tcpLayer->getTcpHeader()->ackNumber) + " \nSyn: " +
                              std::to_string(tcpLayer->getTcpHeader()->sequenceNumber);

            return;
        }

        /* handshake step 3 */
        if (tcpLayer->getTcpHeader()->synFlag == 0 && tcpLayer->getTcpHeader()->ackFlag == 1)
        {
            // set arrow comments
            analysisComment = "TCP handshake Step 3:\nAck: " + std::to_string(tcpLayer->getTcpHeader()->ackNumber);
            return;
        }

        /* TCP connection terminated */
        if (tcpLayer->getTcpHeader()->finFlag == 1)
        {
            // set arrow comments
            analysisComment = "TCP Connection Finished";
            return;
        }
    }

    if (packet->isPacketOfType(pcpp::SSL))
    {

        pcpp::SSLLayer *tlsLayer = packet->getLayerOfType<pcpp::SSLLayer>();

        if (tlsLayer == nullptr)
        {
            qDebug() << "Error: no tls layer in tls packet.";
            return;
        }

        if (tlsLayer->getRecordType() != pcpp::SSL_HANDSHAKE)
            return;

        auto *handshakeLayer = dynamic_cast<pcpp::SSLHandshakeLayer *>(tlsLayer);
        int handshakeCount = handshakeLayer->getHandshakeMessagesCount();

        std::string secondComment;
        std::string thirdComment;

        for (int i = 0; i < handshakeCount; i++)
        {
            pcpp::SSLHandshakeMessage *message = handshakeLayer->getHandshakeMessageAt(i);

            /* TLS handshake step 1 */
            if (message->getHandshakeType() == pcpp::SSL_CLIENT_HELLO)
            {
                // set arrow comments
                analysisComment = "Client Hello";
                return;
            }

            /* TLS handshake step 2 */
            if (message->getHandshakeType() == pcpp::SSL_SERVER_HELLO)
            {
                secondComment = secondComment + "Server Hello\n";
                continue;
            }

            /* TLS handshake step 2 */
            if (message->getHandshakeType() == pcpp::SSL_CERTIFICATE)
            {
                secondComment.append("Certificate\n");
                continue;
            }

            /* TLS handshake step 2 */
            if (message->getHandshakeType() == pcpp::SSL_SERVER_DONE)
            {
                secondComment.append("Server Hello Done\n");
                continue;
            }

            /* TLS handshake step 3 */
            if (message->getHandshakeType() == pcpp::SSL_CLIENT_KEY_EXCHANGE)
            {
                thirdComment.append("Client Key Exchange\n");
            }

            if (message->getHandshakeType() == pcpp::SSL_FINISHED)
            {
                thirdComment.append("Finished\n");
            }
        }

        if (secondComment != "")
            analysisComment = secondComment;
        if (thirdComment != "")
            analysisComment = thirdComment;
    }

    if (!analysisComment.empty())
        emitNewAnalysisComment(index, analysisComment);

    return;
    // void CaptureAnalyseHandshakes::analyseTLSHandshake(pcpp::Packet packet);
    // {
    //     if (!packet->isPacketOfType(pcpp::SSL))
    //         return;

    //     pcpp::SSLLayer *tlsLayer = packet->getLayerOfType<pcpp::SSLLayer>();

    //     if (tlsLayer == nullptr)
    //     {
    //         qDebug() << "Error: no tls layer in tls packet.";
    //         return;
    //     }

    //     if (tlsLayer->getRecordType() != pcpp::SSL_HANDSHAKE)
    //         return;

    //     auto *handshakeLayer = dynamic_cast<pcpp::SSLHandshakeLayer *>(tlsLayer);
    //     int handshakeCount = handshakeLayer->getHandshakeMessagesCount();

    //     std::string secondComment;
    //     std::string thirdComment;

    //     for (int i = 0; i < handshakeCount; i++)
    //     {
    //         pcpp::SSLHandshakeMessage *message = handshakeLayer->getHandshakeMessageAt(i);

    //         /* TLS handshake step 1 */
    //         if (message->getHandshakeType() == pcpp::SSL_CLIENT_HELLO)
    //         {
    //             // set arrow comments
    //             analysisComment = "Client Hello";
    //             return;
    //         }

    //         /* TLS handshake step 2 */
    //         if (message->getHandshakeType() == pcpp::SSL_SERVER_HELLO)
    //         {
    //             secondComment = secondComment + "Server Hello\n";
    //             continue;
    //         }

    //         /* TLS handshake step 2 */
    //         if (message->getHandshakeType() == pcpp::SSL_CERTIFICATE)
    //         {
    //             secondComment.append("Certificate\n");
    //             continue;
    //         }

    //         /* TLS handshake step 2 */
    //         if (message->getHandshakeType() == pcpp::SSL_SERVER_DONE)
    //         {
    //             secondComment.append("Server Hello Done\n");
    //             continue;
    //         }

    //         /* TLS handshake step 3 */
    //         if (message->getHandshakeType() == pcpp::SSL_CLIENT_KEY_EXCHANGE)
    //         {
    //             thirdComment.append("Client Key Exchange\n");
    //         }

    //         if (message->getHandshakeType() == pcpp::SSL_FINISHED)
    //         {
    //             thirdComment.append("Finished\n");
    //         }
    //     }

    //     if (secondComment != "")
    //         analysisComment = secondComment;
    //     if (thirdComment != "")
    //         analysisComment = thirdComment;
    // }
    // }

    // void CaptureAnalyseHandshakes::analyseTCPThreeWayHandshake()
    // {
    //     /* parse the packet */
    //     pcpp::Packet packet(rawPacketList.at(index));

    //     if (!packet->isPacketOfType(pcpp::TCP))
    //         return;

    //     pcpp::TcpLayer const *tcpLayer = packet->getLayerOfType<pcpp::TcpLayer>();

    //     if (tcpLayer == nullptr)
    //     {
    //         qDebug() << "Error: no tcp layer in tcp packet.";
    //         return;
    //     }

    //     /* handshake step 1 */
    //     if (tcpLayer->getTcpHeader()->synFlag == 1 && tcpLayer->getTcpHeader()->ackFlag == 0)
    //     {
    //         // set arrow comments
    //         analysisComment = "TCP handshake Step 1:\nSyn: " +
    //                           std::to_string(tcpLayer->getTcpHeader()->sequenceNumber);
    //         return;
    //     }

    //     /* handshake step 2 */
    //     if (tcpLayer->getTcpHeader()->synFlag == 1 && tcpLayer->getTcpHeader()->ackFlag == 1)
    //     {
    //         // set arrow comments
    //         analysisComment = "TCP handshake Step 2: \nAck: " +
    //                           std::to_string(tcpLayer->getTcpHeader()->ackNumber) + " \nSyn: " +
    //                           std::to_string(tcpLayer->getTcpHeader()->sequenceNumber);

    //         return;
    //     }

    //     /* handshake step 3 */
    //     if (tcpLayer->getTcpHeader()->synFlag == 0 && tcpLayer->getTcpHeader()->ackFlag == 1)
    //     {
    //         // set arrow comments
    //         analysisComment = "TCP handshake Step 3:\nAck: " + std::to_string(tcpLayer->getTcpHeader()->ackNumber);
    //         return;
    //     }

    //     /* TCP connection terminated */
    //     if (tcpLayer->getTcpHeader()->finFlag == 1)
    //     {
    //         // set arrow comments
    //         analysisComment = "TCP Connection Finished";
    //         return;
    //     }
    // }

    // void CaptureAnalyseHandshakes::analyseTLSHandshake()
    // {
    //     if (!packet->isPacketOfType(pcpp::SSL))
    //         return;

    //     pcpp::SSLLayer *tlsLayer = packet->getLayerOfType<pcpp::SSLLayer>();

    //     if (tlsLayer == nullptr)
    //     {
    //         qDebug() << "Error: no tls layer in tls packet.";
    //         return;
    //     }

    //     if (tlsLayer->getRecordType() != pcpp::SSL_HANDSHAKE)
    //         return;

    //     auto *handshakeLayer = dynamic_cast<pcpp::SSLHandshakeLayer *>(tlsLayer);
    //     int handshakeCount = handshakeLayer->getHandshakeMessagesCount();

    //     std::string secondComment;
    //     std::string thirdComment;

    //     for (int i = 0; i < handshakeCount; i++)
    //     {
    //         pcpp::SSLHandshakeMessage *message = handshakeLayer->getHandshakeMessageAt(i);

    //         /* TLS handshake step 1 */
    //         if (message->getHandshakeType() == pcpp::SSL_CLIENT_HELLO)
    //         {
    //             // set arrow comments
    //             analysisComment = "Client Hello";
    //             return;
    //         }

    //         /* TLS handshake step 2 */
    //         if (message->getHandshakeType() == pcpp::SSL_SERVER_HELLO)
    //         {
    //             secondComment = secondComment + "Server Hello\n";
    //             continue;
    //         }

    //         /* TLS handshake step 2 */
    //         if (message->getHandshakeType() == pcpp::SSL_CERTIFICATE)
    //         {
    //             secondComment.append("Certificate\n");
    //             continue;
    //         }

    //         /* TLS handshake step 2 */
    //         if (message->getHandshakeType() == pcpp::SSL_SERVER_DONE)
    //         {
    //             secondComment.append("Server Hello Done\n");
    //             continue;
    //         }

    //         /* TLS handshake step 3 */
    //         if (message->getHandshakeType() == pcpp::SSL_CLIENT_KEY_EXCHANGE)
    //         {
    //             thirdComment.append("Client Key Exchange\n");
    //         }

    //         if (message->getHandshakeType() == pcpp::SSL_FINISHED)
    //         {
    //             thirdComment.append("Finished\n");
    //         }
    //     }

    //     if (secondComment != "")
    //         analysisComment = secondComment;
    //     if (thirdComment != "")
    //         analysisComment = thirdComment;
    // }
}