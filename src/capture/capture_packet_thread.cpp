#include "capture_packet_thread.h"

CapturePacketThread::CapturePacketThread(bool liveCapture,
                                         pcpp::RawPacketVector *rawPacketList,
                                         QVector<QString> *packetCommentList,
                                         QStatusBar *statusBar,
                                         std::string *activeDevName)
{
    this->isLiveCapture = liveCapture;
    this->rawPacketList = rawPacketList;
    this->packetCommentList = packetCommentList;
    this->statusBar = statusBar;
    this->activeDevName = activeDevName;

    setDeviceInfo(*activeDevName);
}

CapturePacketThread::~CapturePacketThread()
{
    if (isLiveCapture)
    {
        stopLiveCapture();
    }
}

void CapturePacketThread::setDeviceInfo(const std::string &devName)
{
    dev = pcpp::PcapLiveDeviceList::getInstance().getPcapLiveDeviceByName(devName);
    // headerinfo.linklayer = dev->getLinkType();
    headerinfo.devname = dev->getName();
    headerinfo.ipv4addr = dev->getIPv4Address();
    headerinfo.macaddr = dev->getMacAddress();
    headerinfo.ipv4addr = dev->getDefaultGateway();
    headerinfo.devdesc = dev->getDesc();
    headerinfo.dnsservers = dev->getDnsServers();
    headerinfo.mtu = dev->getMtu();
    headerinfo.pcaplibver = pcpp::PcapLiveDevice::getPcapLibVersionInfo();
    headerinfo.devtype = dev->getDeviceType();
    headerinfo.filecomment = tr("Capture device: ").toStdString() + headerinfo.devname + ", " +
                             tr("IpV4 Addr: ").toStdString() + headerinfo.ipv4addr.toString() + " , " +
                             tr("LinkLayer Type (").toStdString() + std::to_string(headerinfo.linklayer) + "), library [" +
                             headerinfo.pcaplibver + "]";
}

void CapturePacketThread::run()
{
    qDebug() << "Starting live capture...";
    dev = pcpp::PcapLiveDeviceList::getInstance().getPcapLiveDeviceByName(*activeDevName);

    // qDebug() << "FileComment: " << QString::fromStdString(headerinfo.filecomment);

    /* error if dev is empty */
    if (dev == nullptr)
    {
        qDebug() << "Cannot find device  " << &activeDevName;
        exit();
        return;
    }

    /*
        open the device before start capturing packets
        Note this requires capabilities or sudo privs:
        - CAP_NET_RAW to access network packets, and
        - CAP_NET_ADMIN for access in promiscous mode
    */
    if (!dev->open())
    {
        emit emitCaptureSessionSignal(en::threadState::err_devOpen);
        qCritical() << "Cannot open device: " << &activeDevName;
        qInfo() << "Stopping capture thread ";
        // emit emitCaptureSessionSignal(en::threadState::err_devOpen);
        exit();
        return;
    }

    /* set a filter if provided */
    if (capturefilter != nullptr && !dev->setFilter(capturefilter.toStdString()))
    {
        /* warn if error in filter but continue to capture live packets */
        qInfo() << "Cannot set a live filter: " << capturefilter;
        emit emitCaptureSessionSignal(en::threadState::err_filter);
    }

    if (dev->startCapture(onPacketArrives, this))
    {
        isLiveCapture = true;
        qDebug() << "CPT: started live capture 87";
        emit emitCaptureSessionSignal(en::threadState::ok_startLive);
    }
    else
    {
        isLiveCapture = false;
        qDebug() << "CPT: error during live packet capture 93";
        emit emitCaptureSessionSignal(en::threadState::err_startLive);
        qInfo() << "Stopping capture thread ";
        exit();
        return;
    }
}

void CapturePacketThread::consumePacket(pcpp::RawPacket *rawPacket)
{
    /* append new raw packet to packet list */
    rawPacketList->pushBack(rawPacket);

    /* initialise packet comment */
    packetCommentList->append("");

    // signal that new packet available to render
    emit emitNewCapturePacket(rawPacketList->size());
}

void CapturePacketThread::onPacketArrives(pcpp::RawPacket *rawPacket,
                                          pcpp::PcapLiveDevice *device,
                                          void *cookie)
{
    // extract the consumer object form the cookie
    auto *pThis = (CapturePacketThread *)cookie;

    // save a copy of the raw packet
    auto *rawPacketCopy = new pcpp::RawPacket(*rawPacket);

    // pass the copied raw packet to the consumer
    pThis->consumePacket(rawPacketCopy);
}

void CapturePacketThread::openCaptureLive(const QString &filter)
{
    if (filter != nullptr && !filter.trimmed().isEmpty())
        capturefilter = filter;
    else
        capturefilter = nullptr;
    start();
    return;
}

void CapturePacketThread::stopLiveCapture()
{
    qDebug() << "CPT: Signal to stop live capture";
    emit emitCaptureSessionSignal(en::threadState::ok_stopLive);
    isLiveCapture = false;
    dev->stopCapture();
}

void CapturePacketThread::stopFileLoading()
{
    qDebug() << "CPT: Signal to stop file load";
    isFileLoading = false;
    isFileLoadStopped = true;
}
bool CapturePacketThread::openCaptureFile(const QString &fileName,
                                          const QString &filter)
{

    qDebug() << "Opening file: " << fileName;

    /* open a pcap/pcapng file for reading */
    pcpp::IFileReaderDevice *reader = pcpp::IFileReaderDevice::getReader(fileName.toStdString());
    if (!reader->open())
    {
        qCritical() << "Cannot determine reader for this file type: " << fileName;
        delete reader;
        emit emitCaptureSessionSignal(en::threadState::err_readerOpen);
        return false;
    }

    /* set a filter if provided */
    if (filter != nullptr && !filter.trimmed().isEmpty())
    {
        if (!reader->setFilter(filter.toStdString()))
        {
            /* warn if error in filter but continue to read file */
            qCritical() << "Cannot set filter: " << filter;

            emit emitCaptureSessionSignal(en::threadState::err_filter);
        }
    }

    /* get file extension format to call correct function */
    cmm::fileFormat filetype = cmm::getFileFormat(QFileInfo(fileName).suffix());

    bool ret = false;

    if (filetype == cmm::pcapng)
    {
        auto *pcapNgReader = dynamic_cast<pcpp::PcapNgFileReaderDevice *>(reader);
        headerinfo.os = pcapNgReader->getOS();
        headerinfo.app = pcapNgReader->getCaptureApplication();
        headerinfo.hw = pcapNgReader->getHardware();
        headerinfo.filecomment = pcapNgReader->getCaptureFileComment();
        ret = readPcapNgCaptureFile(pcapNgReader);
    }
    else if (filetype == cmm::pcap)
    {
        headerinfo.clear();

        auto *pcapReader = dynamic_cast<pcpp::PcapFileReaderDevice *>(reader);
        headerinfo.linklayer = pcapReader->getLinkLayerType();

        ret = readPcapCaptureFile(pcapReader);
    }
    else
    {
        QFileInfo f = fileName;
        qCritical() << "Unsupported file type: '" << f.completeSuffix() << "'";
        emit emitCaptureSessionSignal(en::threadState::err_fileType);
    }

    if (ret)
    {
        /* create the stats object to read stats*/
        pcpp::IPcapDevice::PcapStats stats;
        reader->getStatistics(stats);

        qInfo() << "Read" << stats.packetsRecv << " packets successfully and "
                << stats.packetsDrop << " packets could not be read";
    }
    /* close the file */
    reader->close();
    qDebug() << "File closed";
    /* free reader memory */
    delete reader;
    return ret;
}

bool CapturePacketThread::readPcapNgCaptureFile(pcpp::PcapNgFileReaderDevice *reader)
{
    isFileLoading = true;
    isFileLoadStopped = false;
    pcpp::RawPacket rawPacket;
    std::string packetComment;

    while (reader->getNextPacket(rawPacket, packetComment) && isFileLoading == true)
    {
        /* make copy of packet as it will be overwritten on next read */
        auto *rawPacketCopy = new pcpp::RawPacket(rawPacket);

        /* add packet comment */
        packetCommentList->append(QString::fromStdString(packetComment));
        /* add new raw packet to packet list */
        rawPacketList->pushBack(rawPacketCopy);

        /* signal that new packet available to render */
        emit emitNewCapturePacket(rawPacketList->size());
    }

    if (isFileLoadStopped)
    {
        qInfo() << "File load stopped by user";
        emit emitCaptureSessionSignal(en::threadState::ok_stopFile);
        return false;
    }

    return true;
}

bool CapturePacketThread::readPcapCaptureFile(pcpp::IFileReaderDevice *reader)
{
    isFileLoading = true;
    isFileLoadStopped = false;
    pcpp::RawPacket rawPacket;

    while (reader->getNextPacket(rawPacket) && isFileLoading == true)
    {
        // make copy of packet as it will be overwritten on next read
        auto *rawPacketCopy = new pcpp::RawPacket(rawPacket);

        /* insert empty packet comment */
        packetCommentList->push_back("");

        // add new raw packet to packet list
        rawPacketList->pushBack(rawPacketCopy);

        // signal that new packet available to render
        emit emitNewCapturePacket(rawPacketList->size());
    }

    if (isFileLoadStopped)
    {
        qInfo() << "File load stopped by user";
        emit emitCaptureSessionSignal(en::threadState::ok_stopFile);
        return false;
    }

    return true;
}

/*
    Saves packets and any comments to file
*/
bool CapturePacketThread::saveToPcapNgFile(const QString &savefileName,
                                           bool isCompression)
{
    qInfo() << "Saving packets to '" << savefileName << "'";
    if (isCompression)
        qInfo() << "--being saved in compressed format";

    int compressionFactor = 0;
    if (isCompression)
    {
        compressionFactor = 10;
    }

    /*
    create a pcap-ng file writer. Specify file name. Link type is not necessary because
    pcap-ng files can store multiple link types in the same file
    */
    qDebug() << "Opening file for writing: " << savefileName;
    pcpp::PcapNgFileWriterDevice pcapNgWriter(savefileName.toStdString(), compressionFactor);

    if (!pcapNgWriter.open(QSysInfo::prettyProductName().toStdString(),
                           QSysInfo::currentCpuArchitecture().toStdString(),
                           QCoreApplication::applicationName().toStdString(),
                           headerinfo.filecomment))
    {
        qCritical() << "Cannot open  " << savefileName << " for writing";
        emit emitCaptureSessionSignal(en::threadState::err_pcapngReaderOpen);
        return false;
    }

    qInfo() << "Saving packets with comments";

    int c = 0;

    for (auto const *packet : *rawPacketList)
    {
        QString comment = packetCommentList->at(c);
        // qDebug() << "writing comment=" << QString::fromStdString(comment);
        comment = "";

        if (!pcapNgWriter.writePacket(*packet, comment.toStdString()))
        {
            qDebug() << "Cannot write packets to " << savefileName;
            emit emitCaptureSessionSignal(en::threadState::err_writing);
            return false;
        }
        c++;
    }

    /* create the stats object to read stats*/
    pcpp::IPcapDevice::PcapStats stats;
    pcapNgWriter.getStatistics(stats);
    qInfo() << "Written " << stats.packetsRecv << " packets successfully to file and "
            << stats.packetsDrop << " packets could not be written";

    qDebug() << "File closed";
    pcapNgWriter.close();
    return true;
}

bool CapturePacketThread::saveToPcapFile(const QString &savefileName)
{

    qInfo() << "Saving packets to Pcap file: " << savefileName;
    // create a pcap file writer. Specify file name. Link type is necessary because
    // pcap files cannot store multiple link types in the same file

    // TO DO get link layer type to pass to this call. note it dfaults to ethernet
    pcpp::PcapFileWriterDevice pcapWriter(savefileName.toStdString());

    // try to open the file for writing
    if (!pcapWriter.open())
    {
        qCritical() << "Cannot open  " << savefileName << " for writing";
        emit emitCaptureSessionSignal(en::threadState::err_pcapReaderOpen);
        return false;
    }

    if (!pcapWriter.writePackets(*rawPacketList))
    {
        qCritical() << "Cannot write packets to " << savefileName;
        emit emitCaptureSessionSignal(en::threadState::err_writing);
        return false;
    }

    /* create the stats object to read stats*/
    pcpp::IPcapDevice::PcapStats stats;
    pcapWriter.getStatistics(stats);
    qInfo() << "Written " << stats.packetsRecv << " packets successfully to file and "
            << stats.packetsDrop << " packets could not be written";

    pcapWriter.close();
    return true;
}
