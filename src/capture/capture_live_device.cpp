#include "capture_live_device.h"

CaptureLiveDevice::CaptureLiveDevice() = default;

QVector<QStringList> CaptureLiveDevice::getAvailableDevices() const
{
    QVector<QStringList> devicesList;
    QStringList device;
    /* get available devices */
    const auto &devList = pcpp::PcapLiveDeviceList::getInstance().getPcapLiveDevicesList();
    for (const auto &dev : devList)
    {
        // Dev structure:
        // 1] name 2] IPv4 3] Gateway 4] linklayer 5] ? IPv6

        device.clear();
        device.append(QString::fromStdString(dev->getName()));
        device.append(QString::fromStdString(dev->getIPv4Address().toString()));
        device.append(QString::fromStdString(dev->getDefaultGateway().toString()));
        device.append(pkl::getLinkLayerName(dev->getLinkType()));
        device.append("");

        /* check iterface name or if is not empty */
        if (QString::fromStdString(dev->getIPv4Address().toString()) == "0.0.0.0" || QString::fromStdString(dev->getIPv4Address().toString()) == "127.0.0.1")
        {
            continue;
        }
        devicesList.append(device);
    }
    return devicesList;
}

QStringList CaptureLiveDevice::getDeviceList(QString &deviceName)
{
    QVector<QStringList> devicesList;
    QStringList device;
    const auto &devList = pcpp::PcapLiveDeviceList::getInstance().getPcapLiveDevicesList();
    for (const auto &dev : devList)
    {
        if (dev->getName() == deviceName.toStdString())
        {
            device.clear();
            device.append(QString::fromStdString(dev->getName()));
            device.append(QString::fromStdString(dev->getIPv4Address().toString()));
            device.append(QString::fromStdString(dev->getDefaultGateway().toString()));
            device.append(pkl::getLinkLayerName(dev->getLinkType()));
            pcpp::IPv6Address ipv6 = getDeviceIPv6Addr(*dev);

            if (ipv6 == nullptr)
                device.append("");
            else
                device.append(QString::fromStdString(ipv6.toString()));
        }
    }
    return device;
}

QStringList CaptureLiveDevice::getDefaultDevice() const
{
    auto devicesList = getAvailableDevices();
    QStringList defaultDevice;

    /* no selected device inputted defualt to the first in list of available devices */
    if (!(devicesList.first()).empty())
    {
        defaultDevice = devicesList.first();
    }
    return defaultDevice;
}
QString CaptureLiveDevice::getDeviceName(QStringList device) const
{
    QString deviceName;

    /* no selected device inputted defualt to the first in list of available devices */
    if (!(device.at(0)).isEmpty())
    {
        deviceName = device.at(0);
    }
    return deviceName;
}

pcpp::IPv6Address CaptureLiveDevice::getDeviceIPv6Addr(pcpp::PcapLiveDevice const &device)
{

    return nullptr;
}