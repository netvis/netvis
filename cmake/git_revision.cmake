# the following was cribbed from https://www.mattkeeter.com/blog/2018-01-06-versioning/
message ("Obtaining git rev-list --count head value and building header file")
message("-- Configuring " ${VAR})

# make script portable by using Find_Package to get git executable
find_package(Git)
if(Git_FOUND)
  message("-- Git found: ${GIT_EXECUTABLE}")
endif()

execute_process(COMMAND ${GIT_EXECUTABLE} rev-list --count HEAD
                OUTPUT_VARIABLE GIT_REV
                ERROR_QUIET)

# Check whether we got any revision (which isn't
# always the case, e.g. when someone downloaded a zip
# file from Github instead of a checkout)
if ("${GIT_REV}" STREQUAL "")
    set(GIT_REV "beta")
    set(GIT_TAG "N/A")
    set(GIT_BRANCH "N/A")
else()
    execute_process(
        COMMAND ${GIT_EXECUTABLE} describe --exact-match --tags
        OUTPUT_VARIABLE GIT_TAG ERROR_QUIET)
    execute_process(
        COMMAND ${GIT_EXECUTABLE} rev-parse --abbrev-ref HEAD
        OUTPUT_VARIABLE GIT_BRANCH)

    string(STRIP "${GIT_REV}" GIT_REV)
    string(STRIP "${GIT_TAG}" GIT_TAG)
    string(STRIP "${GIT_BRANCH}" GIT_BRANCH)
endif()

# get build time 

string(TIMESTAMP BUILD_TIME "%d%b%Y %H:%M" UTC)

set(VERSION "#ifndef GIT_VERSION_H
#define GIT_VERSION_H

#define GIT_REV \"${GIT_REV}\"
#define GIT_TAG \"${GIT_TAG}\"
#define GIT_BRANCH \"${GIT_BRANCH}\"
#define APP_BUILD_DATE \"${BUILD_TIME}\"

#endif")

# message("-- CMAKE_CURRENT_SOURCE_DIR=" ${CMAKE_CURRENT_SOURCE_DIR})
# message("-- CMAKE_CURRENT_BINARY_DIR=" ${CMAKE_CURRENT_BINARY_DIR})

# if(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/include/git_build.h)
#     file(READ ${CMAKE_CURRENT_SOURCE_DIR}/include/git_build.h VERSION_)
# else()
#     set(VERSION_ "")
# endif()

# if (NOT "${VERSION}" STREQUAL "${VERSION_}")
#     file(WRITE ${CMAKE_CURRENT_SOURCE_DIR}/include/git_build.h "${VERSION}")
# endif()

if(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/${VAR})
    file(READ ${CMAKE_CURRENT_SOURCE_DIR}/${VAR} VERSION_)
else()
    set(VERSION_ "")
endif()

if (NOT "${VERSION}" STREQUAL "${VERSION_}")
    message("-- creating ${CMAKE_CURRENT_SOURCE_DIR}/${VAR}")
    file(WRITE ${CMAKE_CURRENT_SOURCE_DIR}/${VAR} "${VERSION}")
else()
    message("-- file already uptodate: ${CMAKE_CURRENT_SOURCE_DIR}/${VAR}")
endif()